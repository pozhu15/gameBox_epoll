//队列函数
/*
Queue *InitQueue()		//构造一个空队列
{
	Queue *pqueue = (Queue *)malloc(sizeof(Queue));
	if (pqueue != NULL)
	{
		pqueue->front = NULL;
		pqueue->rear = NULL;
		pqueue->size = 0;
	}
	return pqueue;
}

void DestroyQueue(Queue *pqueue)//销毁一个队列
{
	if (IsEmpty(pqueue) != 1)
		ClearQueue(pqueue);
	free(pqueue);
}

void ClearQueue(Queue *pqueue)//清空一个队列
{
	while (IsEmpty(pqueue) != 1)
	{
		DeQueue(pqueue, NULL);
	}

}

int IsEmpty(Queue *pqueue)	//判断队列是否为空
{
	if (pqueue->front == NULL&&pqueue->rear == NULL&&pqueue->size == 0)
		return 1;
	else
		return 0;
}

int GetSize(Queue *pqueue)	//返回队列大小
{
	return pqueue->size;
}

PNode GetFront(Queue *pqueue, Item *pitem)	//返回队头元素
{
	if (IsEmpty(pqueue) != 1 && pitem != NULL)
	{
		memcpy(pitem, &(pqueue->front), Mlong);
		// *pitem = pqueue->front->data;  
	}
	return pqueue->front;
}

PNode GetRear(Queue *pqueue, Item *pitem)	//返回队尾元素
{
	if (IsEmpty(pqueue) != 1 && pitem != NULL)
	{
		//*pitem = pqueue->rear->data; 
		memcpy(pitem, &(pqueue->rear),Mlong);
	}
	return pqueue->rear;
}

PNode EnQueue(Queue *pqueue, Item item)	//将新元素入队
{
	PNode pnode = (PNode)malloc(messL);
	if (pnode != NULL)
	{
		memcpy(&(pnode->Msg), &item, messL);
		pnode->isr=0;
		//pnode->data = item;  
		pnode->next = NULL;

		if (IsEmpty(pqueue))
		{
			pqueue->front = pnode;
		}
		else
		{
			pqueue->rear->next = pnode;
		}
		pqueue->rear = pnode;
		pqueue->size++;
	}
	return pnode;
}

PNode DeQueue(Queue *pqueue, Item *pitem)	//队头元素出队
{
	PNode pnode = pqueue->front;
	if (IsEmpty(pqueue) != 1 && pnode != NULL)
	{
		if (pitem != NULL)
			memcpy(pitem, &pnode->Msg,messL);
		// *pitem = pnode->data;  
		pqueue->size--;
		pqueue->front = pnode->next;
		free(pnode);
		if (pqueue->size == 0)
			pqueue->rear = NULL;
	}
	return pqueue->front;
}

PNode IsOverLimited(Queue *pqueue)	//判断是否超过消息队列最大长度，超出则删除前面已读元素
{
	if(!IsEmpty(pqueue))
	{
		while(pqueue->size>MessLong)
		{
			PNode pnode = pqueue->front;
			if(pnode->isr==1)	//消息已读
			{
				pqueue->size--;
				pqueue->front = pnode->next;
				free(pnode);
			}
		}	
	}
	return pqueue->front;
}
*/


#include "SocketC.h"
/*
int pthread_create (pthread_t *__restrict __newthread,//新创建的线程ID
			  __const pthread_attr_t *__restrict __attr,//线程属性
			  void *(*__start_routine) (void *),//新创建的线程从start_routine开始执行
			  void *__restrict __arg)//执行函数的参数


void init();//初始化登录界面  
void*ReceiveMessage(void*arg);//线程执行函数，接受消息
void*SendMessage(void*arg);//线程执行函数，发送消息
int exact(struct message a);
int main1()
{
	SSocket = &SSock;
	Mesg = fillmess("mima");
	
	socketinit(SSocket);//初始化套接字
	system("clear");
	
	time_t timep;
	struct tm*pt;
	time(&timep);
	pt = gmtime(&timep);
	printf("%d/%d/%d %d:%d",(1900+pt->tm_year),(1+pt->tm_mon),pt->tm_mday,pt->tm_hour+8,pt->tm_min);
	connectS(SSocket);//连接服务器
	
	scanf("%s",na);
	memcpy(Mesg.name,na,30);
	sendS(SSocket,Mesg);//发送数据
	
	
	
	//Mesg = fillmess(na);
	//printf("        密码\n");
	//printf("        注册！\n\n");
	
	
	pthread_t tid1,tid2;
	int err;
	void*tret;
	err = pthread_create(&tid1,NULL,SendMessage,NULL);//创建线程
	if(err!=0)
	{
		printf("pthread_create error:%s\n",strerror(err));
		exit(-1);
	}
	err = pthread_create(&tid2,NULL,ReceiveMessage,NULL);
	if(err!=0)
	{
		printf("pthread_create error:%s\n",strerror(err));
		exit(-1);
	}
	err = pthread_join(tid1,&tret);//阻塞等待线程id为tid1的线程，直到该线程退出
	if(err!=0)
	{
		printf("can not join with thread1:%s\n",strerror(err));
		exit(-1);
	}
	printf("thread 1 exit code %d\n",(int)tret);
	err = pthread_join(tid2,&tret);
	if(err!=0)
	{
		printf("can not join with thread1:%s\n",strerror(err));
		exit(-1);
	}
	printf("thread 2 exit code %d\n",(int)tret);
	
	
	
	
	init();
	closesocket(SSocket);//关闭套接字
	return 0;
}


void*ReceiveMessage(void*arg)//线程执行函数，接受消息
{
	while(1)
	{
		memset(&Mesg,0,Mlong);//清空发送缓存
		Mesg = receiveS(SSocket);//接受数据		
		if(strlen(Mesg.mess)>0)
			//for(int i=0;i<Mesg.length;i++)
		{
			if(Mesg.num==100)
			{
				if(strcmp(Mesg.mess,"clear")==0||strcmp(Mesg.mess,"\nclear")==0)
				{
					system("clear");
					printf("稍安勿躁，管理员以为你清屏！\n");
				}
				else
				{
					printf("管理:%s\n",Mesg.mess);
				}
			}
			else if(Mesg.num==101)//机器人
			{
				printf("%s\n",Mesg.mess);
			}
			else
			{
				printf("%s:(%s)%s\n",Mesg.name,Mesg.Time,Mesg.mess);
			}
		}
		//printf("\n");
	}
	return((void*)0);
}
void*SendMessage(void*arg)//线程执行函数，发送消息
{
	while(1)
	{
		//printf("请输入名称！\n");
		//	if(exact(Mesg)==1)
		{
			//printf("发送！\n");
			scanf("%s",&Mesg.mess);
			if(strcmp(Mesg.mess,"#c")==0)
			{
				system("clear");
			}
			else
			{
				Mesg.num = 1;
				memcpy(Mesg.name,na,30);
				Mesg.length = strlen(Mesg.mess);
				sendS(SSocket,Mesg);//发送数据
			}
		}
	}
	return((void*)0);
}

int exact(struct message a)
{
	switch(a.num)
	{
	case-1:
		switch(a.concrol[0])
		{
		case 10:
			printf("请输入名称！\n");
			scanf("%s",na);
			memcpy(Mesg.name,na,sizeof(na));
			sendS(SSocket,Mesg);//发送数据
			break;
		case 11:
			printf("请输入密码:\n");
			scanf("%s",mima);
			memcpy(Mesg.mess,mima,sizeof(mima));
			sendS(SSocket,Mesg);//发送数据
			break;
		}
		break;
	case 0:
		switch(a.concrol[0])
		{
		case 12:
			printf("请输入密码:\n");
			scanf("%s",mima);
			memcpy(Mesg.mess,mima,sizeof(mima));
			sendS(SSocket,Mesg);//发送数据
			break;
		case 13:
			printf("请输入名称！\n");
			scanf("%s",na);
			memcpy(Mesg.name,na,sizeof(na));
			sendS(SSocket,Mesg);//发送数据
			break;
		}
		break;
	case 1:
		return 1;
		break;
	}
	return 0;
}
void init()//初始化登录界面
{
	printf("        1.贪吃蛇！\n\n");
	printf("        2.聊天室\n");
	printf("        3.待新建！\n\n");
}

void snake()
{
	// memset(Write.mess,0,sizeof(Write.mess));//清空缓存
}
*/