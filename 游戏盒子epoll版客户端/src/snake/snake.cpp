//
#include"../function.h"


void snakeTouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[])
{	char v[20]="";
			 sprintf(v,"%f",x);
			 showToastText(v,0);
	event(action, x, y);
}
void snakeDraw(int left,int top,int right,int bottom,Canvas canvas)		//画图		
{
	drawColor(RGB(125, 125, 155));
	if (snakeheadnode != NULL)
	{
	   traverse_list(snakeheadnode);
	}
	drawtype();
	
}

void snakeCreate()	//在]界面启动时调用
{
	//获取屏幕宽度
	mScreenW=getScreenWidth();
	//屏幕高度
	mScreenH=getScreenHeight();
	//屏幕像素密度
	mScreenD=getScreenDensity();
	snakeheadnode = NULL;
	kzfx = 0;//控制方向
	foodx = 0;//食物坐标
	foody = 0;
	score = 0;//分数
	setTextSize(TEXTH);
	if (snakeheadnode == NULL)
	{
	   init();
	}
}
void snakeSizeChange(int w,int h,int oldw,int oldh,float density)//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
{
	mScreenW=w;
	mScreenH=h;
	mScreenD=density;
	if (snakeheadnode == NULL)
	{
	   init();
	}
}
void snakeLoopCall()	//每1毫秒就会调用这个方法1次
{
	start_timer();
}
int snakeBackPressed()//返回键按下被调用
{
	funnumber=0;
	snakeDestroy();
	prefunnumber=funnumber;
	functiononCreate(funnumber);	//在界面启动时调用	
}
void snakeDestroy()//界面销毁被调用
{
	delte_list(snakeheadnode);
}

//初始化蛇头
void headnodeinit(NODE** p)
{
	*p = (struct Node* )malloc(sizeof(struct Node));
	memset(*p, 0, sizeof(struct Node));
	(*p)->Prior = (*p)->Next = *p;
	
	NODE* q = *p;
	int i = 0;
	for(i = 0; i < 3; i++)
	{
		add_list(q);     
	}

}
//遍历
void traverse_list(NODE* p)
{
	if(p->Next == p)
	{
		return;
	}
	else
	{
		
		NODE* q = p->Next;
		NODE per;
		per = *q;
		NODE t;
		per.r = 0;
		per.g = 255;
		per.b = 0;
		if(kzfx == 0)
		{   
			q->x += SWH;
			q->w += SWH;
		}
		else if(kzfx == 1)		
		{
			
			q->x -= SWH;
			q->w -= SWH;
		}
		else if(kzfx == 2)
		{
			q->y += SWH;
			q->h += SWH;
		}
		else
		{
			q->y -= SWH;
			q->h -= SWH;			
		}
		int x = q->x;
		int y = q->y;     
		q->r = 250;
		q->b = 0;
		q->g = 0;
		drawSnake(q);
		q = q->Next;
		while(q != p)
		{ 
			   t = *q;
			   per.Next = q->Next;
			   *q = per;
			   if((x == q->x && y == q->y) || x >= mScreenW || -1 > x || 
			   y >= mScreenH-100 || -1 > y)
			   {
				  delte_list(snakeheadnode);			  
				  headnodeinit(&snakeheadnode);
				  foodrand();
				  score = 0;
				  return;
			   }
			   per = t;
		   
		   drawSnake(q);
		   q = q->Next;
		}	
		snakeScore();
		food();
		 
		if(isCollRect(x, y, x+SWH, y+SWH, foodx, foody ,foodx+SWH, foody+SWH))
		{
		   foodrand();
		   add_list(p);
		   score += 10;
		}   
	}
 
}
//食物坐标
void foodrand()
{
   do
   {
	   foodx = rand()%(mScreenW - (SWH+SWH));
	   foody = rand()%(mScreenH - (SWH+SWH+100));
   }while(foodx <= SWH || foody <= SWH);
}

//清空链表
void delte_list(NODE* head)
{
	if(head->Next == head)
	{
	 
		return;
	}
	else
	{
		NODE* p = head->Next;
		NODE* q = NULL;  
		while(p != head)
		{
			q = p->Next;
			free(p);
			p = q;		
		}
		free(head);
	}
 
}
//画蛇
void drawSnake(NODE* p)
{
	setColor(RGB(0, 0, 255));
	//drawRect(p->x, p->y, p->w, p->h);
	drawRoundRect(p->x , p->y, p->w, p->h,(p->w-p->x)/2, (p->h-p->y)/2);
	setColor(RGB(p->r, p->g, p->b));
	//drawRect(p->x+5, p->y+5, p->w-5, p->h-5); 
	drawRoundRect(p->x+5, p->y+5, p->w-5, p->h-5,(p->w-p->x-10)/2, (p->h-p->y-10)/2);
}
//添加一节到尾部
void add_list(NODE* p)
{  
	NODE* Pnew = (struct Node* )malloc(sizeof(struct Node));

	Pnew->x = mScreenW/2;
	Pnew->y = mScreenH/2;
	Pnew->w = mScreenW/2+SWH;
	Pnew->h = mScreenH/2+SWH;
	Pnew->r = 0;
	Pnew->g = 255;
	Pnew->b = 0;

	p->Prior->Next = Pnew;
	Pnew->Next = p;
	Pnew->Prior = p->Prior;
	p->Prior = Pnew;
}
//食物
void food()
{
   setColor(RGB(0, 255, 0));
   drawRect(foodx, foody, foodx+SWH, foody+SWH);
   setColor(RGB(250, 0, 0));
   drawRect(foodx+5, foody+5, foodx+SWH-5, foody+SWH-5);
}

void drawtype()
{
	int i;
	float x = 0;
	char* text[4] = {"上", "下", "左", "右"};
	for (i = 0; i < 4; i++)
	{
	   setColor(RGB(0, 0, 0));
	   drawRect(x, mScreenH - 150, x+mScreenW/4, mScreenH);	 
	   setColor(RGB(255, 255, 255));	   
	   drawText(text[i], (x+mScreenW/8), mScreenH - 75);
	   x += mScreenW/4+10;
	}
}
void start_timer()
{
	   static long long proTime=0;
	//每200毫秒执行一次
	if(currentTimeMillis()-proTime>200)
	{
		proTime=currentTimeMillis();		
		postInvalidate();//通知系统重新绘图
	}
  
}
//判断是否吃到食物
int isCollRect(int x, int y, int w, int h, int x2, int y2, int w2, int h2)
{
	if ((w >= x2 && x <= w2) && (h >= y2 && y <= h2))
	{
	   return 1;
	}
	return 0;
}
//分数
void snakeScore()
{
	setColor(RGB(0, 0, 0));
	char buf[20];
	sprintf(buf, "得分:%d", score);	
	drawText(buf, 0, TEXTH);
	drawText("测试版", 300, 500);
}
//初始化
int init()
{
	kzfx = 0;
	score = 0;
	headnodeinit(&snakeheadnode);
	foodrand();
	return 0;
}
//触屏按键
void event(int type, float p1, float p2)
{  /*  char v[20]="";
			 sprintf(v,"%f",p1);
			 showToastText(v,0);*/
	if (ACTION_DOWN == type)
	{		//char v[20]="";
			// sprintf(v,"%d,%d,%d,%d",p1,p2,mScreenW,mScreenH);
			//  showToastText(v,0);
		float x = 0;
		int i;
		for (i = 1; i < 5; i++)
		{
		   if (p2 >= mScreenH-150 && x <= p1 && p1 <= x+mScreenW/4)
		   {
			  break;
		   }
		   x += mScreenW/4+10;
		}
		if(1 == i)//上
		{
			if(snakeheadnode->Next->x != snakeheadnode->Next->Next->x)
			{kzfx = 3;}
		}
		if(2 == i)//下
		{
			if(snakeheadnode->Next->x != snakeheadnode->Next->Next->x)
			{kzfx = 2;}
		}
		if(3 == i)//左
		{
			if(snakeheadnode->Next->y != snakeheadnode->Next->Next->y)
			{kzfx = 1;}
		}
		if(4 == i)//右
		{
			if(snakeheadnode->Next->y != snakeheadnode->Next->Next->y)
			{kzfx = 0;}
		}
	
	
	}
	return ;
}