//登录页

#include "../function.h"


void logonCreate()//界面启动时调用
{
	
	logonButton = NULL;
	logonButton = initialButton();//初始化
	
	Rellayout = newRelativeLayout();//创建布局
	setContentView(Rellayout);//设置主控件
	
	editname = newEditText();//创建编辑框
	editpassword = newEditText();
	
	View_setX(editname,mScreenH/2);//设置编辑框X位置
	View_setY(editname,300);//设置编辑框Y位置
	View_setX(editpassword,mScreenH/2);//设置编辑框X位置
	View_setY(editpassword,500);//设置编辑框Y位置
	
	TextView_setTextEx(editname,"");//设置编辑框内文字
	TextView_setTextSize(editname,15);//设置编辑框字体大小
	TextView_setTextEx(editpassword,"");//设置编辑框内文字
	TextView_setTextSize(editpassword,15);//设置编辑框字体大小
	
	rlp2 = newRLayoutParams(WRAP_CONTENT,WRAP_CONTENT);//相对布局参数
	LayoutParams_setWidth(rlp2,400);//设置布局参数宽
	LayoutParams_setHeight(rlp2,100);
	LayoutParams_setWidth(rlp2,400);//设置布局参数宽
	LayoutParams_setHeight(rlp2,100);
	RLayoutParams_addRule(rlp2,RL_CENTER_HORIZONTAL,RL_TRUE);//水平居中
	View_setLayoutParams(editname,rlp2);//设置布局参数
	View_setLayoutParams(editpassword,rlp2);
	ViewGroup_addView(Rellayout,editname);//添加View
	ViewGroup_addView(Rellayout,editpassword);
	postInvalidate();//通知系统重新绘图
	
  
  //  if(checkuser())//验证本地账号信息是否有记录
	{
   //  showToastText("打开文件成功",0);
   //     client.loginIn();
	}
}
void logonTouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[])//触屏事件
{
	int p1 = buttonEvent(logonButton,x,y);
	static float pre_x = 0;
	static float pre_y = 0;
	static int pre_id = 0;
	if(ACTION_DOWN==action)
	{
		setButtonBitmap(logonButton,p1,MmBitmap[3]);
		pre_y = y;
		pre_x = x;
		pre_id = p1;
	}
	if(pre_id==-1)
	{
		return;
	}
	if(p1!=pre_id||ACTION_UP==action)
	{
		setButtonBitmap(logonButton,pre_id,MmBitmap[1]);
	}
	if(ACTION_UP==action)
	{
		PBUTTON p = seekId(logonButton,pre_id);
		if(p!=NULL)
		{
			if(((p->left<=x)&&(p->right>=x)&&(p->top<=y)&&(p->bottom>=y)))
			{
				if(p1/2+1==3)
				{
					if(client.getmymess().repueststatus==0)
					{
						char*np = TextView_getTextEx(editname);//得到用户名编辑框内容
						char*wp = TextView_getTextEx(editpassword);//得到用户名编辑框内容
						if(strlen(np)<1)
							showToastText("账号不能为空",0);
						else if(strlen(np)>19)
							showToastText("账号长度不能超过20",0);
						else if(strlen(wp)<6)
							showToastText("密码不能小于6位",0);
						else if(strlen(wp)>19)
							showToastText("密码不能大于20位",0);
						else
						{
							
							client.changeusermess(np,wp);//改变客户端信息
							recodeusermess();//记录登录信息
							 showToastText("登陆中",0);
							 client.loginIn();
								
							/*    IsOnline = true;
								funnumber = 0;
								logonDestroy();
								prefunnumber = funnumber;
								functiononCreate(funnumber);//在界面启动时调用
							 */
							
						}
					}
				}
				postInvalidate();
				return;
			}
		}
	}
	postInvalidate();
}
void logonDraw(int left,int top,int right,int bottom,Canvas canvas)//画图		
{
	clearButton(logonButton);
	logonmenu();
	traverseListDraw(logonButton);
}
void logonLoopCall()//每1毫秒就会调用这个方法1次
{
	if(client.getmymess().repueststatus==1&&client.getmymess().repueststatus==client.getmymess().status)
	{
		funnumber = 0;
		logonDestroy();
		prefunnumber = funnumber;
		functiononCreate(funnumber);//在界面启动时调用
	}
	postInvalidate();//通知系统重新绘图
}
void logonSizeChange(int w,int h,int oldw,int oldh,float density)//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
{
	mScreenW = w;
	mScreenH = h;
	mScreenD = density;
	clearButton(logonButton);
	logonmenu();
}
int logonBackPressed()//返回键按下被调用
{
	static long long proTime = 0;
	if(currentTimeMillis()-proTime<1000&&currentTimeMillis()-proTime>5)
	{
		logonDestroy();
		exit(0);
		//return;	
	}
	else
	{
		showToastText("再按一次返回键退出",0);
		proTime = currentTimeMillis();
	}
	
}
void logonDestroy()//界面销毁被调用
{
	//以下仅仅删除了c中的引用，但是java中的引用任然存在
	
	ViewGroup_removeAllViews(Rellayout);//移除全部子视图
	deleteRelativeLayout(Rellayout);//删除相对布局
	
	deleteEditText(editname);
	deleteEditText(editpassword);
	
	deleteRLayoutParams(rlp2);
	
	clearButton(logonButton);//清除按钮
	delteButton(&logonButton);//删除按钮
}
//界面
void logonmenu()
{
	int idj = 1,ido = 0,count = 0;//奇偶控件
	char*title[] = {
		"","上面是用户名","下面是密码",
		"第一次使用直接输入用户名和密码即可注册","原谅我不想搞对位置","扫雷","登录",
	};
	
	addButton(logonButton,idj,title[count++],0,0,mScreenW,mScreenH,TEXTSIZE,RGB(255,25,25),"logon.jpg");
	idj+=2;
	addButton(logonButton,idj,title[count++],(mScreenW-mScreenW/10)/2-200,mScreenH/2-100,mScreenW/2+mScreenW/10-200,mScreenH/2+mScreenW/20-100,TEXTSIZE,RGB(0,0,0),"logon1.jpg");
	idj+=2;
	addButton(logonButton,idj,title[count++],(mScreenW-mScreenW/10)/2-200,mScreenH/2,mScreenW/2+mScreenW/10-200,mScreenH/2+mScreenW/20,TEXTSIZE,RGB(0,0,0),"logon1.jpg");
	idj+=2;
	addButton(logonButton,ido,title[count++],(mScreenW-mScreenW/10)/2-200,mScreenH/2+100,mScreenW/2+mScreenW/10-200,mScreenH/2+mScreenW/20+100,TEXTSIZE,RGB(0,0,0),"");
	ido+=2;
	addButton(logonButton,ido,title[count++],(mScreenW-mScreenW/10)/2+100,mScreenH/2+200,mScreenW/2+mScreenW/10+100,mScreenH/2+mScreenW/20+200,TEXTSIZE,RGB(0,0,0),"");
	ido+=2;
	addButton(logonButton,ido,title[count++],(mScreenW-mScreenW/4)/2,mScreenH/6*4,(mScreenW-mScreenW/4)/2+mScreenW/4,mScreenH/3*2+mScreenW/10,TEXTSIZE+30,RGB(50,150,20),"logon1.jpg");
	ido+=2;
	addButton(logonButton,ido,title[count++],(mScreenW-mScreenW/4)/2,mScreenH/6*4,(mScreenW-mScreenW/4)/2+mScreenW/4,mScreenH/3*2+mScreenW/10,TEXTSIZE+30,RGB(50,150,20),"logon1.jpg");
}