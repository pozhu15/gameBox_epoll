//主页
#include "../function.h"

//读取图片
void readBitmap()
{
	MmBitmap[0] = decodeBitmapFromAssets("beijing.jpg");
	MmBitmap[1] = decodeBitmapFromAssets("1.jpg");
	MmBitmap[2] = decodeBitmapFromAssets("2.jpg");
	MmBitmap[3] = decodeBitmapFromAssets("5.jpg");
}
//释放图片
void deleteImg()
{
	int i;
	for(i = 0;i<IMGSIZE;i++)
	{
		if(MmBitmap[i]!=NULL)
		{
			deleteBitmap(MmBitmap[i]);
			MmBitmap[i] = NULL;
		}
	}
}
//界面
void menu(int a)
{
	if(a==1)
	{
		int i;
		int j;
		int idj = 1,ido = 0,count = 0;//奇偶控件
		float top = mScreenH/7;
		float left = mScreenW/12;
		float right = mScreenW/12*7/4;
		float bottom = right;
		char*title[] = {
			"","汇玩盒子","关于",
			"贪吃蛇","聊天室","网与星","五子棋","贝塞尔",
			"你画我猜","待加","待加","待加",
			"待加","待加","待加","待加",
			"待加","待加","待加","待加",
			"待加","待加","最后","","待加","待加","我的",
		};
		
		addButton(headButton,idj,title[count++],0,0,mScreenW,mScreenH,TEXTSIZE,RGB(255,25,25),"beijing.jpg");
		idj+=2;
		addButton(headButton,idj,title[count++],0,0,mScreenW,mScreenH/15,TEXTSIZE+20,RGB(0,0,0),"1.jpg");
		idj+=2;
		addButton(headButton,ido,title[count++],mScreenW/15*13,mScreenH/100,mScreenW/40*39,mScreenH/15-mScreenH/100,TEXTSIZE,RGB(0,0,0),"1.jpg");
		ido+=2;
		for(i = 0;i<5;i++)
		{
			for(j = 0;j<4;j++)
			{
				//结构体指针，id, 文字，x, y, w, h, 字体大小，字体颜色，控件背景
				addButton(headButton,ido,title[count++],left,top,left+right,top+bottom,TEXTSIZE+5,RGB(0,0,0),"1.jpg");
				left+=right+mScreenW/12;
				ido+=2;
			}
			left = mScreenW/12;
			top+=bottom+mScreenW/10;
		}
		addButton(headButton,idj,title[count++],0,mScreenH/14*13,mScreenW,mScreenH,TEXTSIZE+20,RGB(0,0,0),"1.jpg");
		idj+=2;
		addButton(headButton,ido,title[count++],mScreenW/40,mScreenH/14*13+mScreenH/100,mScreenW/15*2,mScreenH-mScreenH/100,TEXTSIZE+10,RGB(0,0,0),"1.jpg");
		ido+=2;
		addButton(headButton,ido,title[count++],mScreenW/2-(mScreenW/15*2-mScreenW/40),mScreenH/14*13+mScreenH/100,mScreenW/2+(mScreenW/15*2-mScreenW/40),mScreenH-mScreenH/100,TEXTSIZE+10,RGB(0,0,0),"1.jpg");
		ido+=2;
		addButton(headButton,ido,title[count++],mScreenW/15*13,mScreenH/14*13+mScreenH/100,mScreenW/40*39,mScreenH-mScreenH/100,TEXTSIZE+10,RGB(0,0,0),"1.jpg");
		//  ido+=2;
		/* setButtonBitmap(headButton, 38, mBitmap[2]);
   setButtonText(headButton, 0, "C", RGB(255, 0, 0), TEXTSIZE+10, "center");
   setButtonText(headButton, 38, "DEL", RGB(255, 0, 0), TEXTSIZE, "center");
   setButtonText(headButton, 24, "+", RGB(0, 0, 230), TEXTSIZE, "center");
   setButtonText(headButton, 29, "-", RGB(0, 0, 230), TEXTSIZE, "center");
   setButtonText(headButton, 34, "×", RGB(0, 0, 230), TEXTSIZE, "center");
   setButtonText(headButton, 39, "÷", RGB(0, 0, 230), TEXTSIZE, "center");
   */
	}
}
void menuCreate()//在程序启动时调用
{
	
	headButton = NULL;
	readBitmap();
	headButton = initialButton();//初始化
	clearButton(headButton);
	menu(1);

}

void menuTouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[])
{
	int p1 = buttonEvent(headButton,x,y);
	static float pre_x = 0;
	static float pre_y = 0;
	static int pre_id = 0;
	if(ACTION_DOWN==action)
	{
		setButtonBitmap(headButton,p1,MmBitmap[3]);
		pre_y = y;
		pre_x = x;
		pre_id = p1;
	}
	if(pre_id==-1)
	{
		return;
	}
	if(p1!=pre_id||ACTION_UP==action)
	{
		setButtonBitmap(headButton,pre_id,MmBitmap[1]);
	}
	if(ACTION_UP==action)
	{
		PBUTTON p = seekId(headButton,pre_id);
		if(p!=NULL)
		{
			if(((p->left<=x)&&(p->right>=x)&&(p->top<=y)&&(p->bottom>=y)))
			{
				funnumber = p1/2+1;
				if(prefunnumber!=funnumber)
				{
					functionDestroy(prefunnumber);//界面销毁被调用
					prefunnumber = funnumber;
					functiononCreate(funnumber);//在界面启动时调用
				}
				postInvalidate();
				return;
			}
		}
	}
	postInvalidate();
}

void menuDraw(int left,int top,int right,int bottom,Canvas canvas)//只能在这里绘图
{
	traverseListDraw(headButton);
}

void menuLoopCall()//每1毫秒就会调用这个方法1次
{
	static long long proTime = 0;
	//每200毫秒执行一次
	//if(currentTimeMillis()-proTime>200)
	{
		proTime = currentTimeMillis();
		postInvalidate();//通知系统重新绘图
	}
}
void menuSizeChange(int w,int h,int oldw,int oldh,float density)//屏幕大小变化被调用
{
	mScreenW = w;
	mScreenH = h;
	mScreenD = density;
	clearButton(headButton);
	menu(1);
}
void menuDestroy()//界面销毁被调用
{
	deleteImg();
	clearButton(headButton);
	delteButton(&headButton);
}
int menuBackPressed()//返回键按下被调用
{
	static long long proTime = 0;
	if(currentTimeMillis()-proTime<1000&&currentTimeMillis()-proTime>5)
	{
		char exitmess[5] = "exit";
		menuDestroy();
		exit(0);
		//return;	
	}
	else
	{
		showToastText("再按一次返回键退出",0);
		proTime = currentTimeMillis();
	}
}
