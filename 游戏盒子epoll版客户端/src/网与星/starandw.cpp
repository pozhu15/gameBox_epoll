//
#include "../function.h"

Bitmap mBitmap;
double MX,MY;
int ise;//是否有触摸事件

STAR star[MAXSTAR];

 

// 初始化星星
void InitStar(int i)
{
	star[i].status = 0;
	star[i].x = rand()%1000;
	star[i].y = rand()%1400;
	int z = rand()%2+1;
	star[i].stepx = pow(-1.0,z)*((rand()%7000)/1000.0+1);
	star[i].stepy = pow(-1.0,z)*((rand()%7000)/1000.0+1);
	star[i].color = (int)(sqrt(pow(star[i].stepx,2)+pow(star[i].stepy,2))*255/6.0+0.5);// 速度越快，颜色越亮
	star[i].color = RGB(0,star[i].color,star[i].color);
}

// 移动星星
void MoveStar(int i)
{
	// 计算新位置
	star[i].x+=star[i].stepx;
	star[i].y+=star[i].stepy;
	if(star[i].x<=1||star[i].x>=999)
	{
		star[i].stepx = -star[i].stepx;
		star[i].x+=2*star[i].stepx;
	}
	if(star[i].y<=1||star[i].y>=1399)
	{
		star[i].stepy = -star[i].stepy;
		star[i].y+=2*star[i].stepy;
	}
	//其他自定义
	if(star[2].x<=1||star[2].x>=799)
	{
		star[2].stepx = -star[2].stepx;
		star[2].x+=2*star[2].stepx;
	}
	if(star[2].y<=1||star[2].y>=1299)
	{
		star[2].stepy = -star[2].stepy;
		star[2].y+=2*star[2].stepy;
	}
}
bool findjinp(int i,double x,double y,double len)//判断第i个星星与x，y相距是否小于len
{
	if(sqrt(pow(star[i].x-x,2)+pow(star[i].y-y,2))<=len)
		return true;
	else
		return false;
}
//在起点为(x1,y1)，终点为(x2,y2)的线段上移动长度为long,得到点(x,y),和所占比例k
void stepgetp(double x1,double y1,double x2,double y2,double lon,double*x,double*y)
{
	
	if(fabs(x2-x1)<0.0001)
		*x = x1;
	else
		*x = ((x2-x1)>0?1:-1)*sqrt(pow((double)lon,2)/(pow((double)(y2-y1)/(x2-x1),2)+1))+x1;
	if(fabs(y2-y1)<0.0001)
		*y = y1;
	else
		*y = ((y2-y1)>0?1:-1)*sqrt(pow((double)lon,2)/(pow((double)(x2-x1)/(y2-y1),2)+1))+y1;
}


void staradCreate()//界面启动时调用
{
	  mBitmap = NULL;
	  MX = 0,MY = 0;
	ise = 0;
	srand((unsigned)time(NULL));
	
	// 初始化所有星星
	for(int i = 0;i<MAXSTAR;i++)
	{
		InitStar(i);
	}
	mBitmap = decodeBitmapFromAssets("month.jpg");

	 
}
void staradTouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[])//触屏事件
{
	//  if(action==ACTION_DOWN)
	{
		//  addp(&bepo,x,y);//添加第一个点
		float x1 = x,y1 = y;
		if(action==ACTION_MOVE)
		{
			if(x1>1&&x1<799&&y1>1&&y1<1299)
				for(int i = 20;i<MAXSTAR;i++)
			{
				if(sqrt(pow(star[i].x-x,2)+pow(star[i].y-y,2))<=LEN*2)
				{
					double bufx,bufy;
					
					stepgetp(x1,y1,star[i].x,star[i].y,LEN,&bufx,&bufy);
					star[i].x = bufx;
					star[i].y = bufy;
					star[i].status = 1;
					MX = x1,MY = y1;
					ise = 1;
					
				}
				
			}
			
			postInvalidate();//通知系统重新绘图 
		}
		if(action==ACTION_UP)
		{
			MX = 0,MY = 0;
			ise = 0;
		}
	}
}
void staradDraw(int left,int top,int right,int bottom,Canvas canvas)//画图		
{
	Canvas_drawColor(canvas,RGB(25,25,25));
	Paint paint = newPaint();//创建画笔
	Paint_setTextSize(paint,32);//设置文字大小
   // Canvas_drawText(canvas,"破竹",star[0].x,star[0].y,paint);//绘制文本
 
   // Canvas_drawColor(canvas,RGB(25,25,25));
  
   Canvas_drawBitmap(canvas,mBitmap,
		0,0,Bitmap_getWidth(mBitmap),Bitmap_getHeight(mBitmap),
	   // star[2].x,star[2].y,star[2].x+200,star[2].y+200,
	   0,0,1100,1400,
		paint
		);
  
	
	
	   
	for(int i = 20;i<MAXSTAR;i++)
	{
		Paint_setColor(paint,RGB(100,20,200));
		Paint_setStrokeWidth(paint,1);
		for(int j = 20;j<MAXSTAR;j++)
		{
			if(findjinp(j,star[i].x,star[i].y,LEN))
			{
				Canvas_drawLine(canvas,star[i].x,star[i].y,star[j].x,star[j].y,paint);
			}
		}
		if(ise==1)
		{
			if(findjinp(i,MX,MY,LEN+5))
				Canvas_drawLine(canvas,star[i].x,star[i].y,MX,MY,paint);
		}
	}
   for(int i = 20;i<MAXSTAR;i++)
	{
		// 画新星星
		//   Canvas_drawColor(canvas,star[i].color);
		Paint_setColor(paint,RGB(250,250,0));
		Canvas_drawCircle(canvas,star[i].x,star[i].y,4,paint);
	}
	 
	Paint_setColor(paint,RGB(0,250,250));
	Canvas_drawText(canvas,"Auther:破竹",star[0].x,star[0].y,paint);   
	Canvas_drawText(canvas,"QQ:1249223183",star[1].x,star[1].y,paint);

	Paint_setTextSize(paint,60);
	Paint_setColor(paint,RGB(255,255,20));
	Canvas_drawText(canvas,"中",star[3].x,star[3].y,paint);
	Canvas_drawText(canvas,"秋",star[4].x,star[4].y,paint);
	Canvas_drawText(canvas,"节",star[5].x,star[5].y,paint);
	Canvas_drawText(canvas,"快",star[6].x,star[6].y,paint);
	Canvas_drawText(canvas,"乐",star[7].x,star[7].y,paint);
	Canvas_drawText(canvas,"中",star[8].x,star[8].y,paint);
	Canvas_drawText(canvas,"秋",star[9].x,star[9].y,paint);
	Canvas_drawText(canvas,"节",star[10].x,star[10].y,paint);
	Canvas_drawText(canvas,"快",star[11].x,star[11].y,paint);
	Canvas_drawText(canvas,"乐",star[12].x,star[12].y,paint);
	Canvas_drawText(canvas,"中",star[13].x,star[13].y,paint);
	Canvas_drawText(canvas,"秋",star[14].x,star[14].y,paint);
	Canvas_drawText(canvas,"节",star[15].x,star[15].y,paint);
	Canvas_drawText(canvas,"快",star[16].x,star[16].y,paint);
	Canvas_drawText(canvas,"乐",star[17].x,star[17].y,paint);
	Canvas_drawText(canvas,"中",star[18].x,star[18].y,paint);
	Canvas_drawText(canvas,"秋",star[19].x,star[19].y,paint);

	deletePaint(paint);//释放画笔
}
void staradLoopCall()//每1毫秒就会调用这个方法1次
{
	  static long long proTime = 0;
	//每100毫秒执行一次
	if(currentTimeMillis()-proTime>10)
	{
		proTime = currentTimeMillis();
		for(int i = 0;i<MAXSTAR;i++)
		{
			MoveStar(i);
		}
		postInvalidate();//通知系统重新绘图
	}
	
}
void staradSizeChange(int w,int h,int oldw,int oldh,float density)//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
{
	
}
int staradBackPressed()//返回键按下被调用
{
	staradDestroy();//界面销毁被调用
	funnumber=0;
	functiononCreate(funnumber);	//在界面启动时调用
	prefunnumber=funnumber;
	return 0;
}
void staradDestroy()//界面销毁被调用
{
	deleteBitmap(mBitmap);
}