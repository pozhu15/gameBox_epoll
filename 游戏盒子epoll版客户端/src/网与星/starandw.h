#ifndef STARANDW_H
#define STARANDW_H

#define MAXSTAR 200 // 星星总数
#define LEN 160
extern Bitmap mBitmap ;
extern double MX,MY;
extern int ise;//是否有触摸事件

typedef struct STAR1
{
	double x;
	double y;//0为未聚集状态，1为聚集状态，2为中心点
	int status;
	double stepx;
	double stepy;
	int color;
}STAR;

extern STAR star[MAXSTAR];

 
	
void staradCreate();	//界面启动时调用
void staradTouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[]);//触屏事件
void staradDraw(int left,int top,int right,int bottom,Canvas canvas);		//画图		
void staradLoopCall();	//每1毫秒就会调用这个方法1次
void staradSizeChange(int w,int h,int oldw,int oldh,float density);//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
int  staradBackPressed();//返回键按下被调用
void staradDestroy();//界面销毁被调用


// 初始化星星
void InitStar(int i);
// 移动星星
void MoveStar(int i);
bool findjinp(int i,double x,double y,double len);//判断第i个星星与x，y相距是否小于len
//在起点为(x1,y1)，终点为(x2,y2)的线段上移动长度为long,得到点(x,y),和所占比例k
void stepgetp(double x1,double y1,double x2,double y2,double lon,double*x,double*y);



#endif