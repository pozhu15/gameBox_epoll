//
#include "../function.h"
#include "intnet.h"

Client client;
UMessList UserMagList;//接受到的文字消息
UMessDisplayQueue UMessDisplayQ;//显示的布局消息队列

RelativeLayout MessBLayout;//消息布局
RLayoutParams MessBtParam;//消息布局参数

int IdNumber;//消息布局id



MessBoxs::MessBoxs(char*namemess,char*messtext,int idbox)
{
	IdName = IdNumber++;
	IdBox = IdNumber++;
	if(IdNumber>1000)
		IdNumber = 300;
	Messbox = newRelativeLayout();//消息气泡布局
	MessName = newTextView();//name控件
	MessageText = newTextView();//MessageText控件
	Textmess = newRelativeLayout();
	
	View_setId(Messbox,IdBox);
	View_setId(MessName,IdName);
	TextView_setTextEx(MessName,namemess);
	TextView_setTextEx(MessageText,messtext);
	//  View_setBackgroundColor(Textmess,RGB(160,25,90));
	View_setBackgroundColor(MessageText,RGB(160,205,90));
	MessboxParams = newRLayoutParams(FILL_PARENT,FILL_PARENT);//消息气泡布局参数
	MessNameParams = newRLayoutParams(WRAP_CONTENT,WRAP_CONTENT);//name控件布局参数
	MessageTextParams = newRLayoutParams(WRAP_CONTENT,WRAP_CONTENT);//name控件布局参数
	TextmessParam = newRLayoutParams(WRAP_CONTENT,WRAP_CONTENT);
	
	LayoutParams_setWidth(MessboxParams,mScreenW);//设置布局参数宽
	LayoutParams_setWidth(TextmessParam,mScreenW/5*4);//设置布局参数宽
	TextView_setTextSize(MessName,20);//设置字体大小
	TextView_setTextSize(MessageText,20);//设置字体大小
	if(strcmp(namemess,client.getmymess().uname)==0)
	{
		RLayoutParams_addRule(MessNameParams,RL_ALIGN_PARENT_RIGHT,RL_TRUE);//对齐父布局的左
		RLayoutParams_addRule(MessageTextParams,RL_ALIGN_PARENT_RIGHT,RL_TRUE);
	}
	else
	{
		RLayoutParams_addRule(MessNameParams,RL_ALIGN_PARENT_LEFT,RL_TRUE);//对齐父布局的左边
		RLayoutParams_addRule(MessageTextParams,RL_ALIGN_PARENT_LEFT,RL_TRUE);
	}
	RLayoutParams_addRule(MessNameParams,RL_ALIGN_PARENT_TOP,RL_TRUE);//对齐父布局的上边
	RLayoutParams_addRule(MessageTextParams,RL_ALIGN_PARENT_BOTTOM,RL_TRUE);// 对齐父布局的下边
	RLayoutParams_addRule(TextmessParam,RL_CENTER_HORIZONTAL,RL_TRUE);// 是否在父布局中水平居中
	RLayoutParams_addRule(TextmessParam,RL_BELOW,IdName);// 对齐谁的下边
	if(idbox!=0)
		RLayoutParams_addRule(MessboxParams,RL_BELOW,idbox);// 对
	View_setLayoutParams(Messbox,MessboxParams);//设置布局参数
	View_setLayoutParams(MessName,MessNameParams);//设置布局参数
	View_setLayoutParams(MessageText,MessageTextParams);//设置布局参数
	
	View_setLayoutParams(Textmess,TextmessParam);//设置布局参数
	ViewGroup_addView(MessBLayout,Messbox);//添加View
	ViewGroup_addView(Messbox,Textmess);//添加View
	ViewGroup_addView(Messbox,MessName);//添加View
	ViewGroup_addView(Textmess,MessageText);//添加View  
}
int MessBoxs::GetId()
{
	return View_getId(Messbox);
}
void MessBoxs::cleanBoxs()
{
	ViewGroup_removeAllViews(Messbox);//移除全部子视图
	//  deleteRelativeLayout(Messbox);
	ViewGroup_removeAllViews(Textmess);//移除全部子视图
	//   deleteRelativeLayout(Textmess);
	deleteRLayoutParams(TextmessParam);
	deleteRLayoutParams(MessboxParams);
	deleteRLayoutParams(MessNameParams);
	deleteRLayoutParams(MessageTextParams);
	deleteTextView(MessName);
	deleteTextView(MessageText);
	View_setVisibility(Messbox,GONE);
	View_setVisibility(Textmess,GONE);
}

//处理并显示消息
void DealMessThreadProc()
{
	for(UMessList::iterator UserIterator = UserMagList.end();
			UserIterator!=UserMagList.begin();
			--UserIterator)
	{
		MessBoxs viewmess((*UserIterator)->name,(*UserIterator)->Message,UMessDisplayQ.size()>0?(UMessDisplayQ.back()).GetId():0);
		UMessDisplayQ.push(viewmess);
		if(UMessDisplayQ.size()>100)//最大存储100条消息
		{
			UMessDisplayQ.front().cleanBoxs();
			UMessDisplayQ.pop();
		}
	}
}
bool checkuser()//验证本地账号信息是否有记录
{
	char Uname[20];
	char Pword[20];
	fstream usermessagen("/sdcard/umaess.bat",ios::in);
	if(!usermessagen.is_open())
	{
		//这里打开登录页面要记录
		return false;
	}
	else
	{
		//showToastText("文件打开成功！", 0);
		char uname[20] = "";
		char pword[20] = "";
		int i = 0,j = 0;
		char ch = ' ';
		while(usermessagen.get(ch))
		{
			if(ch=='\n')
				break;
			uname[i++] = ch-i;
		}
		while(usermessagen.get(ch))
		{
			if(ch=='\n')
				break;
			pword[j++] = ch-j;
		}
		
		client.changeusermess(uname,pword);//改变客户端信息
		usermessagen.close();
		return true;
	}
}
void recodeusermess()//记录用户登录信息到本地
{
	fstream usermessagen;
	usermessagen.open("/sdcard/umaess.bat",ios::out);
	if(usermessagen.is_open())
		;//showToastText("文件不存在，已新建文件！", 0);
	for(int i = 0;client.getmymess().uname[i]!='\0';i++)
	{
		char ch = client.getmymess().uname[i]+i;
		usermessagen<<ch;
	}
	usermessagen<<"\n";
	for(int i = 0;client.getmymess().passw[i]!='\0';i++)
	{
		char ch = client.getmymess().passw[i]+i;
		usermessagen<<ch;
	}
	usermessagen<<"\n";
	usermessagen.close();
}

void intnetCreate()//界面启动时调用
{
	// 连接服务器
	if(client.Connect())
	{
		showToastText("连接到服务器成功",0);
	}
	else
	{
		showToastText("连接到服务器失败",0);
	}
}
void intnetTouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[])//触屏事件
{
	
}
void intnetDraw(int left,int top,int right,int bottom,Canvas canvas)//画图		
{
	
}
void intnetLoopCall()//每1毫秒就会调用这个方法1次
{
   // if(client.getmymess().status==0)
	{
	client.DealReadMessusekx();//处理读进程消息使用快写
	client.DealWriteMessusekx();//处理写进程消息使用快写
	}
	
	//   else 
	{
		//  if(client.getmymess().repueststatus!=1)
		//   client.sendmessage(CHANGESTATUS,client.getmymess().uname,1,(char*)"chamge");//发送消息   
	}
	
	static long long proTime = 0;
	//每100毫秒执行一次
	if(currentTimeMillis()-proTime>3000)
	{
		proTime = currentTimeMillis();
		//  if(funnumber ==3) 
		{
			// if(client.getmymess().repueststatus!=2||client.getmymess().status!=2)
			//  client.sendmessage(CHANGESTATUS,client.getmymess().uname,2,(char*)"chamge");//发送消息
			
		}
		static int conn = 0;
		if(client.getmymess().status<0&&conn<3)
		{
			client.Connect();
			conn++;
		}
		else if(client.getmymess().status<0&&conn==3)
		{
			showToastText("离线进入",0);
			conn++;
			if(funnumber==29)
			{
				funnumber = 0;
				prefunnumber = 0;
				logonDestroy();
				prefunnumber = funnumber;
				functiononCreate(funnumber);//在界面启动时调用
			}
		}
		if(client.getmymess().status==1&&funnumber==29)
		{
			funnumber = 0;
				prefunnumber = 0;
				logonDestroy();
				prefunnumber = funnumber;
				functiononCreate(funnumber);//在界面启动时调用
		
		}
	}
	
}
void intnetSizeChange(int w,int h,int oldw,int oldh,float density)//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
{
	
}
int intnetBackPressed()//返回键按下被调用
{
	
}
void intnetDestroy()//界面销毁被调用
{
	client.Closeusekx();// 退出进程
}