#ifndef  CHATROOM_COMMON_H
#define CHATROOM_COMMON_H

#include <iostream>
#include <list>
#include <queue>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;


// 默认服务器端IP地址
#define SERVER_IP "0.0.0.0"
#define CLIENT_USE_SERVER_IP "203.195.230.11"
//#define CLIENT_USE_SERVER_IP "127.0.0.1"

// 服务器端口号
#define SERVER_PORT 8009

// int epoll_create(int size)中的size
// 为epoll支持的最大句柄数
#define EPOLL_SIZE 10000

// 缓冲区大小40960
#define BUF_SIZE 0xA000
	
// 新用户登录后的欢迎信息
#define SERVER_WELCOME "Welcome you to join in the chat room! there is %d people in this room!"

// 其他用户收到消息的前缀
#define SERVER_MESSAGE "ClientID %d say >> %s"

// 退出系统
#define EXIT "EXIT"

// 提醒你是聊天室中唯一的客户
#define CAUTION "There is only one int the char room!"

//消息类型
#define LOGIN 1001		//客户端登陆
#define CONNECTSUCCEED	1009	//连接成功
#define LOGINSUCCEED 1005		//客户端登陆成功
#define LOGINERROR 1006		//客户端登陆失败
#define LOGOUT 1000		//客户端退出
#define SERVERM 1002		//服务器消息
#define MESSAGE 1003		//普通消息
#define CHANGESTATUS 1004		//改变状态
#define CHANGESTATUSSUCCEED 1007		//改变状态成功
#define SERVERHAVENOTYOU 1008	//已掉线，需重新登陆
#define CLIENTCHANTESAYNUM 1010	//客户端人数变化，通知人数


typedef struct epollMessage	//通信消息
{
	int emstyle; //消息类型
	char uname[20];	//用户
	int status ;	//用户状态，0连接但未登录，1连接且仅在线状态，2聊天室...
	char mess[BUF_SIZE];	//消息体
	//int srandnum;	//随机值
}epmessage;


// 注册新的fd到epollfd中
// 参数enable_et表示是否启用ET模式，如果为True则启用，否则使用LT模式
static void addfd( int epollfd, int fd, bool enable_et )
{
	struct epoll_event ev;
	ev.data.fd = fd;
	ev.events = EPOLLIN;
	if( enable_et )
		ev.events = EPOLLIN | EPOLLET;
	//
	/*
	int epoll_ctl(int epfd, int op, int fd, struct epoll_event *event);
	epoll的事件注册函数。
	epfd是epoll的句柄，即epoll_create的返回值；
	op表示动作：用三个宏表示：
	EPOLL_CTL_ADD：注册新的fd到epfd中；
	EPOLL_CTL_MOD：修改已经注册的fd的监听事件；
	EPOLL_CTL_DEL：从epfd中删除一个fd；
	fd是需要监听的套接字描述符；
	event是设定监听事件的结构体，数据结构如下：
	typedef union epoll_data
	{
		void *ptr;
		int fd;
		__uint32_t u32;
		__uint64_t u64
	}epoll_data_t;
	struct epoll_event 
	{
	  __uint32_t events;  // Epoll events 
	  epoll_data_t data;  // User data variable 
	};
	
	events可以是以下几个宏的集合：
	EPOLLIN ：表示对应的文件描述符可以读（包括对端SOCKET正常关闭）；
	EPOLLOUT：表示对应的文件描述符可以写；
	EPOLLPRI：表示对应的文件描述符有紧急的数据可读（这里应该表示有带外数据到来）；
	EPOLLERR：表示对应的文件描述符发生错误；
	EPOLLHUP：表示对应的文件描述符被挂断；
	EPOLLET： 将EPOLL设为边缘触发(Edge Triggered)模式，这是相对于水平触发(Level Triggered)来说的。
	EPOLLONESHOT：只监听一次事件，当监听完这次事件之后，就会把这个fd从epoll的队列中删除。
	如果还需要继续监听这个socket的话，需要再次把这个fd加入到EPOLL队列里
*/
	epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &ev);
	// 设置socket为nonblocking模式
	// 执行完就转向下一条指令，不管函数有没有返回。
	fcntl(fd, F_SETFL, fcntl(fd, F_GETFD, 0)| O_NONBLOCK);
	//printf("fd added to epoll!\n\n");
}

#endif // CHATROOM_COMMON_H
