#ifndef CHATROOM_CLIENT_H
#define CHATROOM_CLIENT_H

#include <string>
#include "Common.h"

using namespace std;

typedef struct usermess	//用户信息
{
	char uname[20];	//用户名
	char passw[20];	//密码
	int status;	//用户状态，0下线，1在线，2聊天室,3五子棋...
	int repueststatus;	//正在请求的状态
	int Clientnum;	//客户端人数
}userms;

epmessage fillmess(int style,char* name,int sta,char* mess);	//填充消息
void outpmess(epmessage messb);	//输出消息内容

// 客户端类，用来连接服务器发送和接收消息
class Client {

public:
	// 无参数构造函数
	Client();

	// 连接服务器
	bool Connect();

	// 断开连接
	void Close();
	void Closeusekx();//使用快写

	//控制台端使用函数
	bool Start();// 启动客户端
	bool DealMessage();	//处理读进程和写进程
	bool DealWriteMess(); //处理写消息
	bool DealReadMess(); //处理读消息
	
	//快写端使用函数
	bool DealMessageusekx();	//处理读进程和写进程使用快写
	bool DealReadMessusekx(); //处理读进程消息使用快写
	bool DealWriteMessusekx(); //处理写进程消息使用快写
	
	void sendmess(epmessage mess);	//发送消息到队列
	epmessage getmesssend();	//发送消息队列取出消息
	bool sendmessage2(epmessage mess);	//发送消息,成功返回true
	bool sendmessage(int style,char* name,int sta,char* mess);	//发送消息,成功返回true
	void pushmesstorece(epmessage mess);	//消息加入接收队列中
	epmessage recemess();	//接收消息到队列
	void loginIn();	////登陆信息写入队列
	int chectmess(epmessage mess);	//检查消息内容是否有事件变动
	void changeusermess(char* name,char* passw);	//改变用户名和密码
	userms getmymess();	//得到个人信息
	void setrepueststatus(int sta)	;//设置请求的状态
	int getrecemesslistsize()	;//获得消息队列长度

private:

	// 当前连接服务器端创建的socket
	int sock;

	// 当前进程ID
	int pid;
	
	// epoll_create创建后的返回值
	int epfd;
	int epfdftoc;	//子进程事件

	// 创建管道，其中fd[0]用于父进程读，fd[1]用于子进程写
	int pipe_fd[2];
	int pipe_fdftoc[2];	//fifo管道，其中[0]用于父进程写，[1]用于子进程读

	// 表示客户端是否正常工作
	bool isClientwork;

	// 聊天信息缓冲区
	epmessage mmessage;
	userms mymess;	//个人信息
	//用户连接的服务器 IP + port
	struct sockaddr_in serverAddr;
	queue<epmessage> recemessage_list;	//接受消息队列
	queue<epmessage> sendmessage_list;	//发送消息队列
};



#endif //CHATROOM_CLIENT_H
