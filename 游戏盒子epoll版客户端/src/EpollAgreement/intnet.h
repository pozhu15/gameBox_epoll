#ifndef INTNET_H
#define INTNET_H
#include "Client.h"
#include <iostream>

extern Client client;

#include<fstream>
using namespace std;


#define COMMANDMAXC BUF_SIZE


struct UMessageLite
{
   char name[20];
   char Message[COMMANDMAXC];
};
typedef list<UMessageLite*> UMessList;
class MessBoxs{
	public:
	MessBoxs(char* namemess,char* messtext,int idbox);
	int GetId();
	void cleanBoxs();
	private:
	int IdName;
	int IdBox;
	RelativeLayout Messbox;//消息气泡布局
	RelativeLayout Textmess;
	TextView MessName;//name控件
	TextView MessageText;//MessageText控件
	RLayoutParams TextmessParam;
	RLayoutParams MessboxParams;//消息气泡布局参数
	RLayoutParams MessNameParams;//name控件布局参数
	RLayoutParams MessageTextParams;//name控件布局参数
};
typedef queue<MessBoxs> UMessDisplayQueue;

//extern UMessList UserMagList;


//typedef queue<RecvUserMessNode> UMessQueue;
//UMessQueue UMessQue;//接受到的消息队列


extern UMessList UserMagList;//接受到的文字消息
extern UMessDisplayQueue UMessDisplayQ; //显示的布局消息队列

extern RelativeLayout MessBLayout;//消息布局
extern RLayoutParams MessBtParam;//消息布局参数

extern int IdNumber; //消息布局id


void recodeusermess();//记录用户登录信息到本地
bool checkuser();//验证本地账号信息是否有记录


//extern UMessDisplayQueue UMessDisplayQ;
//extern RelativeLayout MessBLayout;//消息布局
//extern RLayoutParams MessBtParam;//消息布局参数


//extern int IdNumber ;//在chatCreate()函数初始化
void DealMessThreadProc();//处理并显示消息

 
extern void intnetCreate();	//界面启动时调用
extern void intnetTouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[]);//触屏事件
extern void intnetDraw(int left,int top,int right,int bottom,Canvas canvas);		//画图		
extern void intnetLoopCall();	//每1毫秒就会调用这个方法1次
extern void intnetSizeChange(int w,int h,int oldw,int oldh,float density);//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
extern int  intnetBackPressed();//返回键按下被调用
extern void intnetDestroy();//界面销毁被调用

#endif