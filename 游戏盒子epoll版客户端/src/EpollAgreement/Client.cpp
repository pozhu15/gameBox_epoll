#include "Client.h"
#include "../base.h"
#define KUAIXIE //快写上打开宏

epmessage fillmess(int style,char*name,int sta,char*mess)//填充消息
{
	epmessage messbuf;
	bzero(&messbuf,sizeof(epmessage));
	messbuf.emstyle = style;
	strcpy(messbuf.uname,name);
	messbuf.status = sta;
	memcpy(messbuf.mess,mess,strlen(mess));
	return messbuf;
}
void outpmess(epmessage messb)//输出消息内容
{
	#ifndef KUAIXIE
	cout<<"##################### message#############################"<<endl;
	cout<<"message stype:"<<messb.emstyle<<endl;//消息类型
	cout<<"username:"<<messb.uname<<endl;
	cout<<"status:"<<messb.status<<endl;
	cout<<"message body:"<<messb.mess<<endl;
	#else
	showToastText(messb.mess,0);
	#endif
}

// 客户端类构造函数
Client::Client(){
	// 初始化要连接的服务器地址和端口
	serverAddr.sin_family = PF_INET;
	serverAddr.sin_port = htons(SERVER_PORT);
	serverAddr.sin_addr.s_addr = inet_addr(CLIENT_USE_SERVER_IP);
	// 初始化socket
	sock = 0;
	// 初始化进程号
	pid = 0;
	// 客户端状态
	isClientwork = true;
	
	mymess.status = -1;//离线状态,0为未登录，1为登陆
	mymess.repueststatus = 0;
	mymess.Clientnum = 0;
	// epool fd
	epfd = 0;
	epfdftoc = 0;
}

void Client::sendmess(epmessage mess)//发送消息到队列
{
	if(mess.emstyle==CHANGESTATUS)
		mymess.repueststatus = mess.emstyle;
	sendmessage_list.push(mess);
}
epmessage Client::getmesssend()//发送消息队列取出消息
{
	epmessage mess = sendmessage_list.front();
	sendmessage_list.pop();
	return mess;
}
void Client::pushmesstorece(epmessage mess)//消息加入接收队列中
{
	recemessage_list.push(mess);
}
epmessage Client::recemess()//取出接收队列中的消息，前提是先验证队列不为空
{
	epmessage mess = recemessage_list.front();
	recemessage_list.pop();
	return mess;
}
void Client::setrepueststatus(int sta)//设置请求的状态
{
	mymess.repueststatus = sta;
}
int Client::getrecemesslistsize()//获得消息队列长度
{
	return recemessage_list.size();
}

// 连接服务器
bool Client::Connect()
{
	//cout << "Connect Server: " << CLIENT_USE_SERVER_IP << " : " << SERVER_PORT << endl;
	
	// 创建socket
	sock = socket(PF_INET,SOCK_STREAM,0);
	if(sock<0){
		#ifndef KUAIXIE
		perror("sock error");
		#else
		showToastText("sock error",0);
		#endif
		
		return false;
	}
	
	// 连接服务端
	if(connect(sock,(struct sockaddr*)&serverAddr,sizeof(serverAddr))<0){
		#ifndef KUAIXIE
		perror("connect error");
		#else
		showToastText("connect error",0);
		#endif
		
		return false;
	}
	
	// 创建管道，其中fd[0]用于父进程读，fd[1]用于子进程写
	if(pipe(pipe_fd)<0){
		#ifndef KUAIXIE
		perror("pipe error");
		#else
		showToastText("pipe error",0);
		#endif
		perror("pipe error");
		return false;
	}
	//fifo管道，其中[0]用于父进程写，[1]用于子进程读
	if(pipe(pipe_fdftoc)<0){
		#ifndef KUAIXIE
		perror("pipe error");
		#else
		showToastText("pipe error",0);
		#endif
		
		return false;
	}
	
	
	// 创建epoll
	epfd = epoll_create(EPOLL_SIZE);
	epfdftoc = epoll_create(EPOLL_SIZE);
	
	if(epfd<0){
		#ifndef KUAIXIE
		perror("epfd error");
		#else
		showToastText("epfd error",0);
		#endif
		
		return false;
	}
	if(epfdftoc<0){
		#ifndef KUAIXIE
		perror("epfdftoc error");
		#else
		showToastText("epfdftoc error",0);
		#endif
		
		return false;
	}
	
	//将sock和管道读端描述符都添加到内核事件表中
	addfd(epfd,sock,true);
	addfd(epfd,pipe_fd[0],true);
	addfd(epfdftoc,pipe_fdftoc[0],true);
	return true;
}
void Client::changeusermess(char*name,char*passw)//改变用户名和密码
{
	strcpy(mymess.uname,name);
	strcpy(mymess.passw,passw);//改变个人信息
}
userms Client::getmymess()//得到个人信息
{
	return mymess;
}
void Client::loginIn()//登陆信息写入队列
{
	epmessage smess;//填充消息
	bzero(&smess,sizeof(epmessage));
	smess.emstyle = LOGIN;
	strcpy(smess.uname,mymess.uname);
	smess.status = 0;
	mymess.repueststatus = 1;
	strcpy(smess.mess,mymess.passw);
	#ifndef KUAIXIE
	cout<<"Logining,please wait...!\n";
	#else
	showToastText("Logining,please wait...!",0);
	#endif
	
	//outpmess(smess);
	//sendmessage(sock,LOGIN,mymess.uname,0, mymess.passw);
	sendmess(smess);//发送消息到队列
}


// 断开连接，清理并关闭文件描述符
void Client::Close(){
	
	if(pid){
		//关闭父进程的管道和sock
		close(pipe_fd[0]);
		close(pipe_fdftoc[1]);
		close(sock);
	}else{
		//关闭子进程的管道
		close(pipe_fd[1]);
		close(pipe_fdftoc[0]);
	}
}
// 断开连接，清理并关闭文件描述符
void Client::Closeusekx(){
	
	//关闭父进程的管道和sock
	close(pipe_fd[0]);
	close(pipe_fdftoc[1]);
	close(sock);
	//关闭子进程的管道
	close(pipe_fd[1]);
	close(pipe_fdftoc[0]);
}
bool Client::sendmessage(int style,char*name,int sta,char*mess)//发送消息,成功返回true
{
	if(style==CHANGESTATUS)
		mymess.repueststatus = style;
	char buf1[sizeof(epmessage)];
	bzero(buf1,sizeof(epmessage));
	epmessage mege = fillmess(style,name,sta,mess);
	memcpy(buf1,&mege,sizeof(epmessage));
	if(send(sock,buf1,sizeof(epmessage),0)<0)
	{
		return false;
	}
	return true;
}
bool Client::sendmessage2(epmessage mess)//发送消息,成功返回true
{
	if(mess.emstyle==CHANGESTATUS)
		mymess.repueststatus = mess.emstyle;
	char buf1[sizeof(epmessage)];
	bzero(buf1,sizeof(epmessage));
	memcpy(buf1,&mess,sizeof(epmessage));
	if(send(sock,buf1,sizeof(epmessage),0)<0)
	{
		return false;
	}
	return true;
}
int Client::chectmess(epmessage mess)//检查消息内容是否有事件变动
{
	if(mess.emstyle==CHANGESTATUSSUCCEED)//改变状态成功
	{
		mymess.status = mess.status;
		return 1;
	}
	if(mess.emstyle==SERVERHAVENOTYOU)//没有登陆成功
	{
		mymess.status = 0;
		return 2;
	}
	if(mess.emstyle==LOGINSUCCEED)//登陆成功
	{
		mymess.status = mess.status;
		
		return 3;
	}
	if(mess.emstyle==LOGINERROR)//登陆失败
	{
		mymess.status = 0;
		return 4;
	}
	if(mess.emstyle==CONNECTSUCCEED)//连接成功
	{
		mymess.status = 0;
		return 5;
	}
	if(mess.emstyle==CLIENTCHANTESAYNUM)
	{
		mymess.Clientnum = mess.status;
		return 6;
	}
	return 0;
}
// 启动客户端
bool Client::Start()
{
	// 创建子进程
	pid = fork();
	// 如果创建子进程失败则退出
	if(pid<0){
		perror("fork error");
		close(sock);
		return false;
	}
	else if(pid==0)//处理写消息
	{
		// 进入子进程执行流程
		//子进程负责写入管道，因此先关闭读端
		close(pipe_fd[0]);
		close(pipe_fdftoc[1]);
		//while(DealWriteMess());
	}
	else
		//处理读消息
	{
		//pid > 0 父进程
		//父进程负责读管道数据，因此先关闭写端
		close(pipe_fd[1]);
		close(pipe_fdftoc[0]);
		//while(DealReadMess());
	}
	return true;
}

bool Client::DealMessage()//处理读进程和写进程
{
	while(DealWriteMess()&&DealReadMess())
		;
	return false;
}

bool Client::DealWriteMess()//处理写进程消息
{
	static struct epoll_event eventsftoc[3];
	if(pid==0)
	{
		// 如果客户端运行正常则不断读取输入发送给服务端
		if(isClientwork)
		{
			for(int j = 0;j<120;j++)
			{
				int epoll_events_count = epoll_wait(epfdftoc,eventsftoc,3,1);
				//处理就绪事件
				for(int i = 0;i<epoll_events_count;++i)
				{
					//有父进程发来的消息 	
					if(eventsftoc[i].data.fd==pipe_fdftoc[0])
					{
						//父进程从管道中读取数据
						char buf[sizeof(epmessage)];
						epmessage messg;
						bzero(&messg,sizeof(epmessage));
						bzero(buf,sizeof(epmessage));
						int ret = read(pipe_fdftoc[0],buf,sizeof(epmessage));
						if(ret==0)
						{
							perror("fifo read fork error");
							isClientwork = 0;
						}
						else
						{
							memcpy(&messg,buf,sizeof(epmessage));
							switch(chectmess(messg))
							{
							case 1:
								cout<<"system:change status succeed!"<<endl;
								// 输入exit可以退出聊天室
								cout<<"Please input 'exit' to exit the chat room"<<endl;
								break;
							case 2:
								cout<<"system:Sorry because some errors,it will login again!"<<endl;
								break;
							case 3:
								cout<<"system:login succeed!"<<endl;
								break;
							case 4:
								cout<<"system:login failed!"<<endl;
								break;
							case 5:
								cout<<"system:connect succeed!"<<endl;
								break;
							case 6:
								cout<<messg.uname<<" "<<messg.mess<<",now there are "<<mymess.Clientnum<<" people online"<<endl;
								break;
							}
						}
					}
				}
				if(mymess.status==-1)
					sleep(1);//如果已发送登陆。等待2秒
				else
					break;
			}
			if(mymess.status==-1)
			{
				cout<<"Sorry connect to server failed,please try again!"<<endl;
				return true;
			}
			if(!sendmessage_list.empty())//发送消息队列不为空则取出消息
			{
				char buf[sizeof(epmessage)];
				bzero(buf,sizeof(epmessage));
				epmessage mess = sendmessage_list.front();
				sendmessage_list.pop();
				memcpy(buf,&mess,sizeof(epmessage));
				if(write(pipe_fd[1],buf,sizeof(epmessage))<0)
				{
					perror("write fork error");
					exit(-1);
				}
			}
			if(mymess.status!=mymess.repueststatus)
			{
				static int p = 0;
				if(p<60)
				{
					sleep(1);//如果已发送登陆。等待2秒
					p++;
					return true;
				}
				p = 0;
			}
			if(mymess.status==0)//需要登陆
			{
				mymess.repueststatus = 1;
				if(mymess.status==0)
				{
					//cout<<"you are off-line,will you login in? (y/n)"<<endl;
					char iny = 'y';
					//cin>>iny;
					//changeusermess(mymess.uname,char* passw);	//改变用户名和密码
					if(iny=='y')
					{
						loginIn();//登陆信息写入队列
					}
					return true;
				}
			}
			//	continue;	//后面为控制台端测试不再执行
			epmessage messg;
			bzero(&messg,sizeof(epmessage));
			char buf[sizeof(epmessage)];
			bzero(buf,sizeof(epmessage));
			//fgets(messg.mess, BUF_SIZE, stdin);
			cin>>messg.mess;
			
			// 客户输出exit,退出
			if(strlen(messg.mess)<1)
				return true;
			if(strncasecmp(messg.mess,EXIT,strlen(EXIT))==0){
				isClientwork = 0;
			}
			// 子进程将信息写入队列
			else
			{
				if(mymess.status==1)
				{
					mymess.repueststatus = 2;
					epmessage sendmessbuf = fillmess(CHANGESTATUS,mymess.uname,2,messg.mess);//改变状态，填充消息
					//outpmess(sendmessbuf);
					sendmess(sendmessbuf);//加入消息队列
					cout<<"changing status, please wait...!"<<endl;
				}
				else
				{
					epmessage sendmessbuf = fillmess(MESSAGE,mymess.uname,mymess.status,messg.mess);//填充消息
					//outpmess(sendmessbuf);
					sendmess(sendmessbuf);//加入消息队列
				}
			}
		}
		else
			return false;
	}
	return true;
}
bool Client::DealReadMess()//处理读进程消息
{
	// epoll 事件队列
	static struct epoll_event events[2];
	if(pid>0)
	{
		// 主循环(epoll_wait)
		if(isClientwork){
			int epoll_events_count = epoll_wait(epfd,events,2,-1);
			
			//处理就绪事件
			for(int i = 0;i<epoll_events_count;++i)
			{
				//服务端发来消息
				if(events[i].data.fd==sock)
				{
					//接受服务端消息
					char buf[sizeof(epmessage)];
					bzero(buf,sizeof(epmessage));
					epmessage mess;
					bzero(&mess,sizeof(epmessage));
					int ret = recv(sock,buf,sizeof(epmessage),0);
					// ret= 0 服务端关闭
					if(ret==0){
						cout<<"Server closed connection: "<<sock<<endl;
						close(sock);
						isClientwork = 0;
					}
					else
					{
						memcpy(&mess,buf,sizeof(epmessage));
						if(mess.emstyle<1000||mess.emstyle>1500)
							continue;
						
						if(chectmess(mess))//如果有状态改变消息，写入子进程
						{
							//outpmess(mess);
							if(write(pipe_fdftoc[1],buf,sizeof(epmessage))<0)
							{
								perror(" fifo write fork error");
								exit(-1);
							}
						}
						else{
							//cout << mess.mess << endl;
							recemessage_list.push(mess);//接收到的消息加入队列
						}
					}
				}
				//子进程写入事件发生，父进程处理并发送服务端
				else if(events[i].data.fd==pipe_fd[0])
				{
					//父进程从管道中读取数据
					char buf[sizeof(epmessage)];
					bzero(buf,sizeof(epmessage));
					int ret = read(events[i].data.fd,buf,sizeof(epmessage));
					if(ret==0)
						isClientwork = 0;
					else{
						epmessage mess;
						bzero(&mess,sizeof(epmessage));
						memcpy(&mess,buf,sizeof(epmessage));
						if(mess.emstyle<1000)
							continue;
						// 将信息发送给服务端
						send(sock,buf,sizeof(epmessage),0);
					}
				}
				if(!recemessage_list.empty())//接收消息队列不为空则输出消息
				{
					epmessage recem = recemess();
					//outpmess(recem);
					cout<<recem.uname<<"："<<recem.mess<<endl;
				}
			}//for
		}//if
		else
			return false;
	}
	return true;
}
bool Client::DealMessageusekx()//处理读进程和写进程使用快写
{
	while(DealReadMessusekx()&&DealWriteMessusekx())
		;
	return false;
}
bool Client::DealWriteMessusekx()//处理写进程消息使用快写
{
	/*   if(mymess.status!=mymess.repueststatus)
	{
		static int p = 0;
		if(p<60)
		{
			sleep(1);//如果已发送登陆。等待2秒
			p++;
			return true;
		}
		p = 0;
	}
	if(mymess.status==0)
		loginIn();
	epmessage messg;
	bzero(&messg,sizeof(epmessage));
	char buf[sizeof(epmessage)];
	bzero(buf,sizeof(epmessage));
	//fgets(messg.mess, BUF_SIZE, stdin);
	cin>>messg.mess;
	
	// 客户输出exit,退出
	if(strlen(messg.mess)<1)
		return true;
	if(strncasecmp(messg.mess,EXIT,strlen(EXIT))==0){
		isClientwork = 0;
	}
	// 子进程将信息写入队列
	else
	{
		if(mymess.status==1)
		{
			mymess.repueststatus = 2;
			epmessage sendmessbuf = fillmess(CHANGESTATUS,mymess.uname,2,messg.mess);//改变状态，填充消息
			//outpmess(sendmessbuf);
			sendmess(sendmessbuf);//加入消息队列
		}
		else
		{
			epmessage sendmessbuf = fillmess(MESSAGE,mymess.uname,mymess.status,messg.mess);//填充消息
			//outpmess(sendmessbuf);
			sendmess(sendmessbuf);//加入消息队列
		}
	}*/
	while(!sendmessage_list.empty())//发送消息队列不为空则发送消息
	{
		epmessage mess = sendmessage_list.front();
		sendmessage_list.pop();
		if(mess.emstyle<1000)
			continue;
		// 将信息发送给服务端
		sendmessage2(mess);//发送消息,成功返回true
	}
	return true;
}
bool Client::DealReadMessusekx()//处理读进程消息使用快写
{
	// epoll 事件队列
	static struct epoll_event events[2];
	// 主循环(epoll_wait)
	int epoll_events_count = epoll_wait(epfd,events,2,0);
	//处理就绪事件
	for(int i = 0;i<epoll_events_count;++i)
	{
		//服务端发来消息
		if(events[i].data.fd==sock)
		{
			//接受服务端消息
			char buf[sizeof(epmessage)];
			bzero(buf,sizeof(epmessage));
			epmessage messg;
			bzero(&messg,sizeof(epmessage));
			int ret = recv(sock,buf,sizeof(epmessage),0);
			// ret= 0 服务端关闭
			if(ret==0){
			   // cout<<"Server closed connection: "<<sock<<endl;
				showToastText("Server closed connection",0);
		 
				close(sock);
				return false;
			}
			else
			{
				memcpy(&messg,buf,sizeof(epmessage));
				if(messg.emstyle<1000||messg.emstyle>1500)
					continue;
				
				if(chectmess(messg))//如果有状态改变消息，写入子进程
				{
					switch(chectmess(messg))
					{
					case 1:
						#ifndef KUAIXIE
						cout<<"system:change status succeed!"<<endl;
						// 输入exit可以退出聊天室
						cout<<"Please input 'exit' to exit the chat room"<<endl;
						#else
						showToastText("change status succeed",0);
						#endif
						
						break;
					case 2:
						#ifndef KUAIXIE
						cout<<"system:Sorry because some errors,it will login again!"<<endl;
						#else
						showToastText("login errors",0);
						#endif
						break;
					case 3:
						#ifndef KUAIXIE
						cout<<"system:login succeed!"<<endl;
						#else
						showToastText("login succeed!",0);
						#endif
						break;
					case 4:
						#ifndef KUAIXIE
						cout<<"system:login failed!"<<endl;
						#else
						showToastText("login failed!",0);
						#endif
						
						break;
					case 5:
						#ifndef KUAIXIE
						cout<<"system:connect succeed!"<<endl;
						#else
						showToastText("connect succeed!",0);
						#endif
						
						break;
					case 6:
						#ifndef KUAIXIE
						cout<<messg.uname<<" "<<messg.mess<<",now there are "<<mymess.Clientnum<<" people online"<<endl;
						#else
						char vbuf[50]="";
						 sprintf(vbuf,"%s %s",messg.uname,messg.mess);
						 showToastText(vbuf,0);
						#endif
						break;
						
					}
				}
				else
				{
					//cout << messg.mess << endl;
					if(messg.emstyle>1000&&messg.emstyle<1500)
						recemessage_list.push(messg);//接收到的消息加入队列
				}
			}
		}
	}//for
	return true;
}
