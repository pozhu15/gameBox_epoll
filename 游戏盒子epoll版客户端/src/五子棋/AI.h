#ifndef AI_H
#define AI_H
#include<math.h>
#include<stdlib.h>
#include<time.h>

#define Npoints 10 //最大分支数 
#define Ntype 17 //棋型类别数 
#define WIN 1 //4000
#define LOSE 2 //-4000
#define FLEX4 3 //2000
#define flex4 4 //-2000
#define BLOCK4 5 //1000
#define block4 6 //-1000 
#define FLEX3 7 //1000 ?
#define flex3 8 //-1000
#define BLOCK3 9 //400 ?
#define block3 10 //-600 ? 
#define FLEX2 11 //400 ?
#define flex2 12 //-600 ?
#define BLOCK2 13 //100 ?
#define block2 14 //-150 ?
#define FLEX1 15 //100 ?
#define flex1 16 //-150 ?

//棋盘
typedef struct Board 
{
	char array[15][15]; //棋盘方阵 0为空点 1为黑 2为白 	
	char type1[15][Ntype]; //15条横线上的棋型 	
	int TYPE1[Ntype]; //所有横线上的总棋型 
	char type2[15][Ntype]; //15条竖线上的棋型 	
	int TYPE2[Ntype]; //所有竖线上的总棋型 	
	char type3[19][Ntype]; //19条平行于副对角线的斜线上的棋型，斜线的最小长度为6 	
	int TYPE3[Ntype]; //所有平行于副对角线的斜线上的总棋型	
	char type4[19][Ntype]; //19条平行于主对角线的斜线上的棋型，斜线的最小长度为6 	
	int TYPE4[Ntype]; //所有平行于主对角线的斜线上的总棋型	
	int eva; //棋盘相对于黑方的局势评估值 	
	char result; //棋盘结局 0为未完 1为黑胜 2为白胜 
}board;
extern board BOARD,ZERO;

typedef struct Route
{
	int x[120];
	int y[120];
	int step;
	char first;
}route;
extern route ROUTE,ZERO_ROUTE;

//将棋盘A黑白交换后写到B 
int inv_board( board &A, board &B);


//棋型辨识数组 
extern int scoreAI[3][3][3][3][3][3]; 
//给棋型辨识数组赋值 
int init_score();


//对棋盘A的局势进行评分 
int evaluate( board &A,int x,int y);

 struct points
{
	int coo_x[Npoints]; //横坐标 
	int coo_y[Npoints]; //纵坐标 
	int eva[Npoints]; //评估值 
	int exi[Npoints]; //存在性 
};

//对于棋盘A，评估出最佳的N个待定落子点 
struct points seek_points( board &A,int N);


typedef struct Decision
{
	int coo_x;
	int coo_y;
	int eva;
	int num;
}decision;

extern decision DECISION;
extern int level_limit;
extern int Nbranch[15];

//博弈树MinMax递归分析 AlphaBeta剪枝 
int analyse( board &A,int level,int alpha,int beta);

void initboard(int x,int y,int type);

/*	AI调用	 */ 
/*
	结构体 BOARD 储存当前棋盘信息 ,数组 BOARD.array[15][15]为当前棋盘的映射, 0代表空，1代表AI的棋子，2代表对手的棋子 
	
	AI调用方法:  analyse(BOARD,0,-1000000,1000000);
	
	AI决策储存于全局变量 DECISION，落子坐标为 ( DECISION.coo_x, DECISION.coo_y ) 
	
	对棋盘落子操作 (假设要落子的坐标为 X,Y)： 
	BOARD.array[X][Y]=1或2；
	evaluate(BOARD,X,Y); 
	
	修改 BOARD.array[15][15]中的元素后，必须调用一次 evaluate(BOARD,x,y); 以更新棋盘的描述信息 
	x,y为被修改的元素的坐标 
	 
*/

/*	AI参数自定义	*/
/*
	全局变量 Nbranch[15] 为AI搜索博弈树时每一层的节点分支数，
	愈大则搜索范围愈广，但程序速度愈慢 。作者猜测采用逐层递减规律时效果较佳 ，其最大值不得超过 宏Npoints 
	
	全局变量 level_limit 为AI搜索博弈树时的最大搜索深度 ，只能为偶数 ，愈大则搜索步数愈多，但程序速度愈慢 

*/ 

#endif