#ifndef FIVEZ_H
#define FIVEZ_H
#include "AI.h"

extern int fortsize1 ;
	 
void fivezCreate();	//界面启动时调用
void fivezTouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[]);//触屏事件
void fivezDraw(int left,int top,int right,int bottom,Canvas canvas);		//画图		
void fivezLoopCall();	//每1毫秒就会调用这个方法1次
void fivezSizeChange(int w,int h,int oldw,int oldh,float density);//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
int  fivezBackPressed();//返回键按下被调用
void fivezDestroy();//界面销毁被调用

void Aievent(int dx,int dy);//由玩家落子后Ai落子

#endif