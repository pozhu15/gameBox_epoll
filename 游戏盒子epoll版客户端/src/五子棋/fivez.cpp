//
#include "../function.h"

int fortsize1 ;
void Aievent(int dx,int dy)//由玩家落子后Ai落子
{
	if(BOARD.array[dx-1][dy-1]==0)
	{
		initboard(dx-1,dy-1,2);
		postInvalidate();//重新绘图
		if(ROUTE.step==1&&abs(dx-8)<3&&abs(dy-8)<3)
		{
			int x = dx,y = dy;
			do
			{
				dx = dx+(double)rand()/RAND_MAX*3-2;
				dy = dy+(int)rand()/RAND_MAX*3-2;
			}while(dx+1==x&&dy+1==y)
				;
			initboard(dx++,dy++,1);
		}
		else
		{
			analyse(BOARD,0,-1000000,1000000);
			initboard(DECISION.coo_x,DECISION.coo_y,1);
			dx = DECISION.coo_x+1;
			dy = DECISION.coo_y+1;
		}
		postInvalidate();//重新绘图
	}
	else
	{
		//cout << "该位置棋子已落"<<endl;
	}
}

void fivezCreate()//界面启动时调用
{
	init_score();
	
	BOARD = ZERO;
	ROUTE = ZERO_ROUTE;
	ROUTE.first = 2;
	//  initboard(7,7,1);
	
	fortsize1 = 50;
}
void fivezTouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[])//触屏事件
{
		//  if(action==ACTION_DOWN)
	{
		if(action==ACTION_UP)
		{
			if(x>60&&x<960&&y>60&&y<960)
			{
				int y1 = y,x1 = x;
				if(!BOARD.result)
					Aievent((y1-30)/60+((y1-30)-(y1-30)/60*60)/30,(x1-30)/60+((x1-30)-(x1-30)/60*60)/30);
			}
			if(x>450&&x<650&&y>1000&&y<1100)
				if(ROUTE.step>1)
			{
				initboard(ROUTE.x[ROUTE.step-1],ROUTE.y[ROUTE.step-1],0);
				initboard(ROUTE.x[ROUTE.step-1],ROUTE.y[ROUTE.step-1],0);
				postInvalidate();//重新绘图
			}
			if(x>700&&x<900&&y>1000&&y<1100)
			{
				BOARD = ZERO;
				ROUTE = ZERO_ROUTE;
				ROUTE.first = 2;
			  //  initboard(7,7,1);
			  fortsize1=50;
				postInvalidate();//重新绘图
			}
		}
	}
}
void fivezDraw(int left,int top,int right,int bottom,Canvas canvas)//画图		
{
	Canvas_drawColor(canvas,RGB(150,155,155));
	Paint paint = newPaint();//创建画笔
	Paint_setTextSize(paint,32);//设置文字大小
	//  Canvas_drawText(canvas,"Hello world!",100,100,paint);//绘制文本
	for(int i = 60;i<=960;i+=60)
	{
		Canvas_drawLine(canvas,i,60,i,960,paint);//画线
	}
	for(int i = 60;i<=960;i+=60)
	{
		Canvas_drawLine(canvas,60,i,960,i,paint);//画线
	}
	for(int i = 0;i<15;i++)
		for(int j = 0;j<15;j++)
	{
		if(BOARD.array[i][j]==1)
		{
			Paint_setColor(paint,RGB(0,0,0));//设置画笔颜色
			Canvas_drawCircle(canvas,j*60+90,i*60+90,30,paint);//画圆
		}
		else if(BOARD.array[i][j]==2)
		{
			Paint_setColor(paint,RGB(255,255,255));//设置画笔颜色
			Canvas_drawCircle(canvas,j*60+90,i*60+90,30,paint);//画圆
		}
	}
	if(BOARD.result)
	{
		Paint_setTextSize(paint,50);//设置文字大小
		Paint_setColor(paint,RGB(0,0,0));
		if(BOARD.result==1)
		{
			Paint_setTextSize(paint,fortsize1);//设置文字大小
			Canvas_drawText(canvas,"你输了!",510-fortsize1*4/2,510-fortsize1/2,paint);
		}
		if(BOARD.result==2)
		{
			Paint_setTextSize(paint,fortsize1);//设置文字大小
			Canvas_drawText(canvas,"你赢了!",510-fortsize1*4/2,510-fortsize1/2,paint);
		}
	}
	RectF rect;
	rect.left = 450;
	rect.top = 1000;
	rect.right = 650;
	rect.buttom = 1100;
	Paint_setColor(paint,RGB(100,100,100));
	Canvas_drawRectEx(canvas,rect,paint);
	Paint_setColor(paint,RGB(0,0,0));
	Paint_setTextSize(paint,70);
	Canvas_drawText(canvas,"悔棋",480,1085,paint);
	RectF rect1;
	rect1.left = 700;
	rect1.top = 1000;
	rect1.right = 900;
	rect1.buttom = 1100;
	Paint_setColor(paint,RGB(100,100,100));
	Canvas_drawRectEx(canvas,rect1,paint);
	Paint_setColor(paint,RGB(0,0,0));
	Paint_setTextSize(paint,70);
	Canvas_drawText(canvas,"重玩",730,1085,paint);
  //  Canvas_drawText(canvas,"作者:破竹",200,1250,paint);
  //  Canvas_drawText(canvas,"QQ:1249223183",200,1350,paint);
	
	deletePaint(paint);//释放画笔
}
void fivezLoopCall()//每1毫秒就会调用这个方法1次
{
	static long long proTime = 0;
	static int i = 3;
	//每100毫秒执行一次
	if(currentTimeMillis()-proTime>1)
	{
		proTime = currentTimeMillis();                   
		postInvalidate();//通知系统重新绘图
		
		fortsize1+=i;
		if(fortsize1>200)
		{         
			 fortsize1=50;
		}
	}

}
void fivezSizeChange(int w,int h,int oldw,int oldh,float density)//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
{
	
}
int fivezBackPressed()//返回键按下被调用
{
	fivezDestroy();
	funnumber = 0;
	functiononCreate(funnumber);//在界面启动时调用	
	prefunnumber = funnumber;
	postInvalidate();//通知系统重新绘图
   // return 0;
}
void fivezDestroy()//界面销毁被调用
{
	
}