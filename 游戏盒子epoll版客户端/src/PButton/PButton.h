#ifndef BUTTON_H_
#define BUTTON_H_
#include "../base.h"
#include <string.h>
#include <stdlib.h>
typedef struct fbutton
{
	int id;//控件id
	float left;//左
	float top;//顶部
	float right;//右
	float bottom;//底部
	char* text;//文字
	float text_x;//文字坐标
	float text_y;
	int textSize;//字体大小
	int textColor;//字体颜色
	int drawRectColor;
	char* bacnkground;//背景颜色
	jobject bitmap;//位图
	char bitmapFlag;
	struct fbutton* Prior;//指向前驱节点
	struct fbutton* Next;//指向后继节点
}FBUTTON, *PBUTTON;

extern PBUTTON initialButton();//初始化
extern PBUTTON addButton(PBUTTON, int, const char* , float, float, float, float, int, int, const char* );//添加控件
extern int traverseListDraw(PBUTTON);//绘图
extern void clearButton(PBUTTON);//清空
extern void delteButton(PBUTTON* );//销毁
extern PBUTTON seekId(PBUTTON, int);//查找id, 返回节点
extern int removeButton(PBUTTON, int);//删除Button
extern int setButtonBitmap(PBUTTON, int, jobject);//替换Button背景图片
extern int setButtonText(PBUTTON, int, const char* , int, int, const char* );//设置Button文本 头指针，id, 文本，颜色，大小，显示位置,center, left，right, top, bottom
extern int setButtonText1(PBUTTON headButton, int id, const char* text, int color, int size, float wf,float hf);//设置Button文本 头指针，id, 文本，颜色，大小，文本显示位置距离按钮左边wf倍,距离按钮顶部hf倍,
extern int setButtonPosition(PBUTTON, int, float, float, float, float);//重新设置Button显示位置 头指针，id, x, y, w, h
extern int buttonEvent(PBUTTON, float, float);//Button按键事件
#endif