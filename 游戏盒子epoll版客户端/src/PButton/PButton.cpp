#include "PButton.h"
#include"../base.h"

static int r_change_ten(const char* , int);
static jobject seekBitmap(PBUTTON , const char* );
static int drawButton(PBUTTON );

//初始化 返回头指针
PBUTTON initialButton()
{
   PBUTTON head = (PBUTTON)malloc(sizeof(FBUTTON));
   head->Next = head->Prior = head;
   
   return head;
}

//16进制转十进制
static int r_change_ten(const char* text, int original)
{
   if(text == NULL || *text == '\0')//空字符串返回0
   {
	   return -1;
   }
   int decimalism = 0;
   char ch = 0;;
   while(*text == '0' || *text == 'x')//跳过字符串开头的
   {  
	  text++;
   }
   int i;
   for (i = 0; text[i] != 0; i++)
   {   ch = text[i];
	   decimalism *= original;
	   if(ch >= '0' && ch <= '9')
	   {   
		   decimalism += ch-'0';
		   continue;
	   }
   
	   if(ch >= 'A' && ch <= 'F')
	   {   
		   decimalism += ch-('A'-10);
		   continue;
	   }
	   
	   if(ch >= 'a' && ch <= 'f')
	   {   
		   decimalism += ch-('a'-10);
		   continue;     
	   }
			  
	   return -1;//没有执行if语句，说明遇到非法字符 返回-1
   }
   return decimalism;
}

//查找位图是否存在过
 static jobject seekBitmap(PBUTTON p, const char* bacnkground)
{
	if (p == NULL || p->Next == p)
	{
	   return NULL;
	}
	PBUTTON q = p->Next;
	PBUTTON q2 = p->Prior;
	do
	{
	   if (strcmp(q->bacnkground, bacnkground) == 0)
	   {
		   return q->bitmap;
	   }else if (strcmp(q2->bacnkground, bacnkground) == 0)
	   {
		  return q2->bitmap;
	   }
	   q = q->Next;
	   if (q == q2)
	   {
		   break;
	   }
	   q2 = q2->Prior;
	}while(q != q2);
	
	return NULL;
}
/*
添加按钮
结构体指针，id, 文字，x, y, w, h, 字体大小，字体颜色，控件背景
返回当前按钮指针
*/

PBUTTON addButton(PBUTTON p, int id, const char* text, float left, float top, float right, float bottom, int textSize, int textColor, const char* bacnkground)
{
   if (p == NULL || text == NULL || bacnkground == NULL)
   {
	  return NULL;
   }
   PBUTTON newButton = (PBUTTON)malloc(sizeof(FBUTTON));
   newButton->id = id;
   int textlen = strlen(text);
   newButton->text = (char* )malloc(textlen+1);
   newButton->text[textlen] = 0;
   strcpy(newButton->text, text);
   textlen = strlen(bacnkground);
   newButton->bacnkground = (char* )malloc(textlen+1);
   newButton->bacnkground[textlen] = 0;
   strcpy(newButton->bacnkground, bacnkground);
   newButton->left = left;
   newButton->top = top;
   newButton->right = right;
   newButton->bottom = bottom;
   newButton->textSize = textSize;
   newButton->textColor = textColor;
   setTextSize(textSize);
   jstring str = createJString(newButton->text);
   float textWidth = measureText(str);
   deleteJString(str);
   newButton->text_x = (left + right)/2-textWidth/2;
   newButton->text_y = (top + bottom)/2 + textSize/2;
   int color = r_change_ten(bacnkground, 16);
   newButton->bitmapFlag = 0;
   if (color != -1)
   {
	   newButton->drawRectColor = (0xff << 24) | color;
	   newButton->bitmap = NULL;
   }
   else
   {
	   newButton->drawRectColor = 0;
	   newButton->bitmap = seekBitmap(p, bacnkground);
	   if (newButton->bitmap == NULL)
	   {
		   jstring name = createJString(newButton->bacnkground);
		   newButton->bitmap = decodeBitmapFromAssets(newButton->bacnkground);
		   deleteJString(name);
		   if (newButton->bitmap != NULL)
		   {
			  newButton->bitmapFlag = 1;
		   }
	   }
	   
   }
	  
   newButton->Next = p;
   newButton->Prior = p->Prior;
   p->Prior->Next = newButton;
   p->Prior = newButton;
   
   return newButton;
}

//绘图
 static int drawButton(PBUTTON p)
{
	if (p == NULL)
	{
	   return 0;
	}
	
	if (p->bitmap != NULL)
	{
		AndroidBitmapInfo bitmapInfo;
		getBitmapInfo(p->bitmap, &bitmapInfo);
		drawBitmap(p->bitmap, 0, 0, bitmapInfo.width, bitmapInfo.height, p->left, p->top, p->right, p->bottom);
	}
	else
	{
		setColor(p->drawRectColor);
		drawRect(p->left, p->top, p->right, p->bottom);
	}
	setColor(p->textColor);
	setTextSize(p->textSize);
	drawText(p->text, p->text_x, p->text_y);
	
	return 1;
}

//进行绘图 头指针
int traverseListDraw(PBUTTON headButton)
{
	if (headButton == NULL || headButton->Next == headButton)
	{
	   return 0;
	}
	 drawColor(RGB(0, 0, 0));
	 PBUTTON q = headButton->Next;
	 while (q != headButton)
	 {
		drawButton(q);
		q = q->Next;
	 }
	 
	return 1;
}
//查找id
PBUTTON seekId(PBUTTON headButton, int id)
{
	if (headButton == NULL || headButton->Next == headButton)
	{
	   return NULL;
	}
	PBUTTON q = headButton->Next;
	PBUTTON q2 = headButton->Prior;
	do
	{
	   if (q->id == id)
	   {
		   return q;
	   }else if (q2->id == id)
	   {
		  return q2;
	   }
	   q = q->Next;
	   if (q == q2)
	   {
		   break;
	   }
	   q2 = q2->Prior;
	}while(q != q2);
	
	return NULL;
}
//删除Button控件, 头指针，id, 删除后自行通知系统重新绘图
int removeButton(PBUTTON headButton, int id)
{
	PBUTTON q = seekId(headButton, id);
	if (q == NULL)
	{
	   return 0;
	}
	
	q->Prior->Next = q->Next;
	q->Next->Prior = q->Prior;
	if (q->text != NULL)
	{
	   free(q->text);
	   q->text = NULL;
	}
	if (q->bitmapFlag == 1)
	{
	   if (q->bitmap != NULL)
	   {
		   if (q->Next != headButton)
		   {
			  PBUTTON b = q->Next;
			  while (b != headButton)
			  {
				 if (strcmp(b->bacnkground, q->bacnkground) == 0)
				 {
					 b->bitmapFlag = 1;
					break;
				 }
				 b = b->Next;
			  }
			  if (b == headButton)
			  {
				  deleteBitmap(q->bitmap);
				  q->bitmap = NULL;
			  }
		   }
		   else
		   {
			  deleteBitmap(q->bitmap);
			  q->bitmap = NULL;
		   }
	   }
	   
	}
	if (q->bacnkground != NULL)
	{
		free(q->bacnkground);
		q->bacnkground = NULL;
	}
	free(q);
	q = NULL;
	
	return 1;
}

//替换Button背景图片，头指针，id, bitmap
int setButtonBitmap(PBUTTON headButton, int id, jobject bitmap)
{
	PBUTTON p = seekId(headButton, id);
	if (p != NULL)
	{
	   if (p->bitmap == bitmap)
	   {
		  return 0;
	   }

	   if (p->bitmapFlag == 1)
	   {
		   if (p->Next != headButton)
		   {
			   char flag = 0;
			  PBUTTON q = p->Next;
			  while (q != headButton)
			  {
				 if (q->bacnkground != NULL && p->bacnkground != NULL)
				 if (strcmp(q->bacnkground, p->bacnkground) == 0)
				 {
					flag = 1;
					break;
				 }
				 q = q->Next;
			  }
			  if (flag == 1)
			  {
				 q->bitmapFlag = 1;
			  }
			  else
			  {
				 deleteBitmap(p->bitmap);
				 p->bitmapFlag = 0;
				 if (p->bacnkground != NULL)
				 {
					free(p->bacnkground);
					p->bacnkground = NULL;
				 }
			  }
		   }
		   else
		   {
			  deleteBitmap(p->bitmap);
			  p->bitmapFlag = 0;
			  if (p->bacnkground != NULL)
			  {
				 free(p->bacnkground);
				 p->bacnkground = NULL;
			  }
		   }
		  
	   }
	   p->bitmap = bitmap;
	   if (p->bacnkground != NULL)
	   {
		  free(p->bacnkground);
		  p->bacnkground = NULL;
	   }
	   p->bitmapFlag = 0;
	   return 1;
	}
	else
	{
	   return 0;
	}
}
//设置Button文本 头指针，id, 文本，颜色，大小，文本显示位置,center, left，right, top, bottom
int setButtonText(PBUTTON headButton, int id, const char* text, int color, int size, const char* gravity)
{
   if (headButton == NULL || headButton->Next == headButton)
   {
	  return 0;
   }
   
   PBUTTON p = seekId(headButton, id);
   if (p != NULL)
   {
	  if (p->text != NULL)
	  {
		 free(p->text);
		 p->text = NULL;
	  }
	  int textLen = strlen(text);
	  p->text = (char* )malloc(textLen+1);
	  strcpy(p->text, text);
	  if (color != 0)
	  {
		 p->textColor = color;
	  }
	  if (size >= 0)
	  {
		 p->textSize = size;
	  }
	  setTextSize(p->textSize);
	  jstring str = createJString(text);
	  int textWidth = measureText(str);
	  if (strcmp(gravity, "center") == 0)
	  {
		 p->text_x = (p->left + p->right)/2 - textWidth/2;
		 p->text_y = (p->top + p->bottom)/2 + size/2;
	  }else if (strcmp(gravity, "left") == 0)
	  {
		 p->text_x = p->left;
		 p->text_y = (p->top + p->bottom)/2 + size/2;
	  }else if (strcmp(gravity, "top") == 0)
	  {
		  p->text_x = (p->left + p->right)/2 - textWidth/2;
		  p->text_y = p->top + size;
	  }else if (strcmp(gravity, "right") == 0)
	  {
		  p->text_x = p->right - textWidth;
		  p->text_y = (p->top + p->bottom)/2 + size/2;
	  }else if (strcmp(gravity, "bottom") == 0)
	  {
		 p->text_x = (p->left + p->right)/2 - textWidth/2;
		 p->text_y = p->bottom;
	  }
	  deleteJString(str);
	  return 1;
   }
   else
   {
	  return 0;
   }
}
//设置Button文本 头指针，id, 文本，颜色，大小，文本显示位置距离按钮左边1/wf,距离按钮顶部1/hf,
int setButtonText1(PBUTTON headButton, int id, const char* text, int color, int size, float wf,float hf)
{
   if (headButton == NULL || headButton->Next == headButton)
   {
	  return 0;
   }
   
   PBUTTON p = seekId(headButton, id);
   if (p != NULL)
   {
	  if (p->text != NULL)
	  {
		 free(p->text);
		 p->text = NULL;
	  }
	  int textLen = strlen(text);
	  p->text = (char* )malloc(textLen+1);
	  strcpy(p->text, text);
	  if (color != 0)
	  {
		 p->textColor = color;
	  }
	  if (size >= 0)
	  {
		 p->textSize = size;
	  }
	  setTextSize(p->textSize);
	  jstring str = createJString(text);
	  int textWidth = measureText(str);
	  p->text_x =p->left+(p->right-p->left-textWidth)/wf;
	  p->text_y =p->top+(p->bottom-p->top-size)/hf;
	  deleteJString(str);
	  return 1;
   }
   else
   {
	  return 0;
   }
}
//重新设置Button显示位置 头指针，id, x, y, w, h
int setButtonPosition(PBUTTON headButton, int id, float left, float top, float right, float bottom)
{
   PBUTTON p = seekId(headButton, id);
   if (p != NULL)
   {
	  p->left = left;
	  p->top = top;
	  p->right = right;
	  p->bottom = bottom;
	  jstring str = createJString(p->text);
	  float textWidth = measureText(str);
	  deleteJString(str);
	  p->text_x = (p->left + p->right)/2 - textWidth/2;
	  p->text_y = (p->top + p->bottom)/2 + p->textSize/2;
	  return 1;
   }
   else
   {
	  return 0;
   }
   
}
//Button按键，成功返回id , 失败返回-1
int buttonEvent(PBUTTON headButton, float x, float y)
{
	if (headButton == NULL || headButton->Next == headButton)
	{
	   return -1;
	}
	PBUTTON q = headButton->Next;
	PBUTTON q2 = headButton->Prior;
	do
	{
	   if ((x >= q->left && x <= q->right) && (y >= q->top && y <= q->bottom))
	   {
		   if(q->id%2!=1)
		   return (q->id);
	   }else if ((x >= q2->left && x <= q2->right) && (y >= q2->top && y <= q2->bottom))
	   {
		  if(q2->id%2!=1)
		  return (q2->id);
	   }
	   q = q->Next;
	   if (q == q2)
	   {
		   break;
	   }
		
	   q2 = q2->Prior;
	}while(q != q2); 
	return -1;
}

//清空Button
void clearButton(PBUTTON p)
{
	if (p == NULL)
	{
	   return;
	}
	if (p->Next != p)
	{
	   PBUTTON q = p->Next;
	   PBUTTON q2 = NULL;
	   while (q != p)
	   {
		  q2 = q->Next;
		  free(q->text);
		  q->text = NULL;
		  free(q->bacnkground);
		  q->bacnkground = NULL;
		  if (q->bitmapFlag == 1)
		  {
			 if (q->bitmap != NULL)
			 {
				deleteBitmap(q->bitmap);
				q->bitmap = NULL;
			 }
		  }
		  free(q);
		  q = q2;
	   }
	   p->Next = p->Prior = p;
	}
}

//销毁Button
void delteButton(PBUTTON* p)
{
   if (*p != NULL)
   {
	  free(*p);
	  *p = NULL;
   }
}
