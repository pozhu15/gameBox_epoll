#ifndef FUNCTION_H
#define FUNCTION_H
//#pragma warning( push [,4])
//#pragma warning( disable : 4705 )
//#pragma warning( disable : 4706 )
//#pragma warning( disable : 4707 )
//.......
//#pragma warning(pop)
#include<math.h>
#include "base.h"
#include "PButton/PButton.h"
#include "块模板/test.h"
#include "EpollAgreement/intnet.h"
#include "网与星/starandw.h"
#include "五子棋/fivez.h"
#include "贝塞尔/bezier.h"


#define IMGSIZE 4	//主页所用图片数
#define TEXTSIZE 40	//主页字体大小

//如果要使用LOGI,和LOGE请定义LOG_TAG这个宏
#define  LOG_TAG    "main"
 
void out();

/******************定义全局变量区******************/
 //屏幕宽度
extern int mScreenW;
//屏幕高度
extern int mScreenH;
//屏幕像素密度
extern float mScreenD;
//程序块调用码
extern int funnumber;		
extern int prefunnumber;
extern int logonnumber;

/*--------主页---------*/
extern jobject MmBitmap[IMGSIZE];
extern PBUTTON headButton;
/*--------网络---------*/

extern Client client;
/*--------关于---------*/
//数据体
extern PBUTTON aboutButton;

struct DearT{
int x;
int y;
int D;
struct DearT* next;
};
extern struct DearT *Deart; 
/*--------贪吃蛇---------*/
//蛇宽高
#define SWH 35
#define TEXTH 32//字体大小

extern int kzfx ;//控制方向
extern int foodx ;//食物坐标
extern int foody ;
extern int score ;//分数
typedef struct Node
{
	int x;
	int y;
	int w;
	int h;
	int r;
	int g;
	int b;
	struct Node* Prior;//指向前驱节点指针域
	struct Node* Next;//指向后继节点指针域
}NODE;

extern NODE* snakeheadnode;


/*--------聊天室---------*/
extern AbsLayout Abslayout  ;  //主绝对布局;
extern RelativeLayout Rellayout1;    //创建相对布局1
extern RelativeLayout Rellayout2;    //创建相对布局2
extern  Button button1 ;
extern  Button button2 ;
extern  Button button3;
extern EditText editmessage;//消息编辑框
extern char messbuf[100];   //消息缓存
/*--------登录---------*/
extern PBUTTON logonButton;
extern RelativeLayout Rellayout;    //创建主相对布局
extern EditText editname;//用户名编辑框
extern EditText editpassword;//密码编辑框
//extern char UserName[20];   //用户名
//extern char PassWord[20];   //密码
extern RLayoutParams rlp ;      //创建主相对布局参数
extern RLayoutParams rlp1 ;      //创建相对布局参数
extern RLayoutParams rlp2 ;      //创建相对布局参数
extern RLayoutParams rlp3 ;      //创建相对布局参数
/*--------关于---------*/


/*--------关于---------*/
  

/******************声明函数区******************/
/*--------系统函数---------*/
//只能在这里绘图
void functiononDraw(int a,int left,int top,int right,int bottom,Canvas canvas);
//触摸事件
void functiononTouchEvent(int a,int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[]);
//在程序启动时调用
void functiononCreate(int a);
//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
void functionSizeChange(int a,int w,int h,int oldw,int oldh,float density);
//系统每1毫秒就会调用这个方法1次
void functiononLoopCall(int a);
//界面销毁，返回键被按下事件，如果 返回0则退出程序，
void functionDestroy(int a);
//返回键被按下事件，如果 返回0则退出程序，
int functionBackPressed(int a);
/*--------主页---------*/
void menuTouchEvent (int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[]);//主菜单接触事件
void menuSizeChange(int w,int h,int oldw,int oldh,float density);	//主菜单屏幕大小发生变化执行
void menu(int a);
void readBitmap();
void deleteImg();
void menuCreate();	//在程序启动时调用
void menuDestroy();//界面销毁被调用
void menuDraw(int left,int top,int right,int bottom,Canvas canvas);//只能在这里绘图
int menuBackPressed();//返回键按下被调用
void menuLoopCall();	//每1毫秒就会调用这个方法1次
/*--------贪吃蛇--------*/
void snakeTouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[]);
void snakeDraw(int left,int top,int right,int bottom,Canvas canvas)	;	//画图		
void snakeCreate();	//在关于界面启动时调用
void snakeSizeChange(int w,int h,int oldw,int oldh,float density);//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
void snakeLoopCall();	//每1毫秒就会调用这个方法1次
void snakeDestroy();//界面销毁被调用
int snakeBackPressed();//返回键按下被调用

void headnodeinit(NODE** );
void add_list(NODE*);
void drawSnake(NODE* );
void food();
void snakeScore();
void delte_list(NODE* );
void traverse_list(NODE*);
int init();
void start_timer();
void drawtype();
void foodrand();
void event(int, float, float);
int isCollRect(int, int, int, int, int, int, int, int);
#define  LOG_TAG    "LOG_TAG"
/*--------关于---------*/
void aboutTouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[]);//关于触屏
void aboutDraw(int left,int top,int right,int bottom,Canvas canvas);//关于绘图
void aboutCreate();		//在程序启动时调用
void aboutDestroy();//界面销毁被调用
int aboutBackPressed();//返回键按下被调用
void aboutSizeChange(int w,int h,int oldw,int oldh,float density);//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
void aboutLoopCall();	//每1毫秒就会调用这个方法1次
void aboutpage(); //界面
void drawdeart(Canvas canvas,int x,int y,double D,Paint paint);//画心，
void involve(double degrees,double px,double py,double x,double y);//旋转

/*--------聊天室---------*/
void chatCreate();	//界面启动时调用
void chatTouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[]);//触屏事件
void chatDraw(int left,int top,int right,int bottom,Canvas canvas);		//画图		
void chatLoopCall();	//每1毫秒就会调用这个方法1次
void chatSizeChange(int w,int h,int oldw,int oldh,float density);//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
int chatBackPressed();//返回键按下被调用
void chatDestroy();//界面销毁被调用
/*--------登录---------*/
void logonCreate();	//界面启动时调用
void logonTouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[]);//触屏事件
void logonDraw(int left,int top,int right,int bottom,Canvas canvas);		//画图		
void logonLoopCall();	//每1毫秒就会调用这个方法1次
void logonSizeChange(int w,int h,int oldw,int oldh,float density);//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
int logonBackPressed();//返回键按下被调用
void logonDestroy();//界面销毁被调用
void logonmenu();//界面
/*--------扫雷---------*/
void findbomCreate();	//界面启动时调用
void findbomTouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[]);//触屏事件
void findbomDraw(int left,int top,int right,int bottom,Canvas canvas);		//画图		
void findbomLoopCall();	//每1毫秒就会调用这个方法1次
void findbomSizeChange(int w,int h,int oldw,int oldh,float density);//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
int findbomBackPressed();//返回键按下被调用
void findbomDestroy();//界面销毁被调用


/*--------关于---------*/


/*--------关于---------*/


/*--------关于---------*/


/*--------关于---------*/


/*--------关于---------*/


/*--------关于---------*/


/*--------关于---------*/



/*--------关于---------*/


/*--------关于---------*/


/*--------关于---------*/






#endif