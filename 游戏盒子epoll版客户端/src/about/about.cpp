//
#include"../function.h"
#include<time.h>

void aboutTouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[])
{	
	int p1 = buttonEvent(aboutButton, x, y);
	static float pre_x = 0;
	static float pre_y = 0;
	static int pre_id = 0;
	if (ACTION_DOWN == action)
	{ 
	   setButtonBitmap(aboutButton, p1, MmBitmap[3]);
	   pre_y = y;
	   pre_x = x;
	   pre_id = p1;
	}
	if (pre_id == -1)
	{
	   return;
	}
	if(p1!=pre_id||ACTION_UP == action)
	{
		setButtonBitmap(aboutButton, pre_id, MmBitmap[1]);	 
	}
	if (ACTION_UP == action)
	{
			PBUTTON p = seekId(aboutButton, pre_id);
			if (p != NULL)
			{
			   if (((p->left <= x) && (p->right >= x) && (p->top <= y) && (p->bottom >= y)))
			   {
				 funnumber=0;//p1/2;
				  if(prefunnumber!=funnumber)
				{
					functionDestroy(prefunnumber);//界面销毁被调用
					functiononCreate(funnumber);	//在界面启动时调用
					prefunnumber=funnumber;
				}
				  postInvalidate();
				  return;
			}
		}
	}
	postInvalidate();
}
void aboutDraw(int left,int top,int right,int bottom,Canvas canvas)				
{
	
	/*	Canvas canvas = newCanvas(mBitmap);//创建画布
	Paint paint = newPaint();//创建画笔
	Paint_setTextSize(paint,32);//设置文字大小
	Canvas_drawText(canvas,"Hello world!",0,32,paint);//绘制文本
	deletePaint(paint);//释放画笔
	deleteCanvas(canvas);//释放画布
	Bitmap_saveToFile(mBitmap,"/sdcard/beijing.jpg");//将位图保存到文件
	*/
	/*Canvas_drawColor(canvas,RGB(150,125,75));
	Paint paint = newPaint();//创建画笔
	Paint_setColor(paint,RGB(0,0,0));//设置画笔颜色
	Canvas_drawPoint(canvas,10,10,paint);//画点
	Canvas_drawLine(canvas,0,100,100,100,paint);//画线
	Canvas_drawCircle(canvas,400,200,100,paint);//画圆
	Paint_setTextSize(paint,32);//设置文字大小
	Paint_setTypeface(paint,TYPEFACE_MONOSPACE);//设置为等宽字体
	Canvas_drawText(canvas,"Hello world!",500,500,paint);//绘制文本
	deletePaint(paint);//释放画笔*/
	
	aboutpage(); //界面
	traverseListDraw(aboutButton);
	
	Paint paint = newPaint();//创建画笔
	Paint_setColor(paint,RGB(0,0,0));//设置画笔颜色
	int text_x,text_y;
	char text[20]="作者：破竹";
	int textLen = strlen(text);
	jstring str = createJString(text);
	int textWidth = measureText(str);
	text_x = (mScreenW-textWidth);
	text_y = (mScreenH-TEXTSIZE+20);
	Paint_setTextSize(paint,TEXTSIZE+20);//设置文字大小
	Canvas_drawText(canvas,text,text_x/2,text_y/10*3,paint);//绘制文本
	strcpy(text,"QQ:1249223183");
	textLen = strlen(text);
	str = createJString(text);
	textWidth = measureText(str);
	text_x = (mScreenW-textWidth);
	text_y = (mScreenH-TEXTSIZE+10);
	Canvas_drawText(canvas,text,text_x/2,text_y/10*4,paint);//绘制文本
	strcpy(text,"快写代码群:136898517");
	textLen = strlen(text);
	str = createJString(text);
	textWidth = measureText(str);
	text_x = (mScreenW-textWidth);
	text_y = (mScreenH-TEXTSIZE+10);
	Canvas_drawText(canvas,text,text_x/2,text_y/10*5,paint);//绘制文本
	strcpy(text,"作者个人群：494594592");
	textLen = strlen(text);
	str = createJString(text);
	textWidth = measureText(str);
	text_x = (mScreenW-textWidth);
	text_y = (mScreenH-TEXTSIZE+10);
	Canvas_drawText(canvas,text,text_x/2,text_y/10*6,paint);//绘制文本
	strcpy(text,"更多新功能正在完善中");
	textLen = strlen(text);
	str = createJString(text);
	textWidth = measureText(str);
	text_x = (mScreenW-textWidth);
	text_y = (mScreenH-TEXTSIZE+10);
	Canvas_drawText(canvas,text,text_x/2,text_y/10*7,paint);//绘制文本
	strcpy(text,"敬请期待!");
	textLen = strlen(text);
	str = createJString(text);
	textWidth = measureText(str);
	text_x = (mScreenW-textWidth);
	text_y = (mScreenH-TEXTSIZE+10);
	Canvas_drawText(canvas,text,text_x/2,text_y/10*8,paint);//绘制文本
	
	srand(time(0));
	Paint_setColor(paint,RGB(250,100,0));//设置画笔颜色
	setStrokeWidth(TEXTSIZE+400);
	for(int x=0, y=0,k=0;k<10;k++)
	{
		x=rand()%mScreenW/50*48+mScreenW/50;
		y=rand()%mScreenH+mScreenH/10;
		drawdeart(canvas, x,y, mScreenW/50,paint);//画心
	}
	deletePaint(paint);//释放画笔*/
	//canvasRotate(float degrees,float px,float py);

	

}
void drawdeart(Canvas canvas,int x,int y,double D,Paint paint)//画心，
{
	// D=300;   //心形大小
	double x1=-1,y1,y2;
	int X1,X2,Y1,Y2;
	  for(x1=0;x1<=1.0;x1+=0.001)
	 {
		 y1=sqrt(1-x1*x1)+pow(x1,2.0/3);
		 y2=-sqrt(1-x1*x1)+pow(x1,2.0/3);
		 X1=x1*D+x;
		  X2=x-D*x1;
		 Y1=y-y1*D;
		  Y2=y-y2*D;
		 Canvas_drawCircle(canvas ,X1,Y1,2,paint); //画圆
		 Canvas_drawCircle(canvas ,X1,Y2,2, paint);
			Canvas_drawCircle(canvas ,X2,Y1,2, paint);
		 Canvas_drawCircle(canvas ,X2,Y2,2, paint);
		  // S+=0xff001100; 
	 }  
 }
 //界面
void aboutpage()
{
   int idj =1,ido=0,count=0;//奇偶控件
   char* title[] = {
	   "","关于","返回",
   "贪吃蛇", "聊天室", "坦克",
   };
	addButton(aboutButton, idj, title[count++], 0, 0,mScreenW, mScreenH, TEXTSIZE+10, RGB(25, 25, 25), "about1.jpg");
	//setButtonText(aboutButton, idj, "作者：守界者" ,  RGB(25, 25, 25), TEXTSIZE+10, "top");//设置Button文本 头指针，id, 文本，颜色，大小，显示位置,center, left，right, top, bottom
	idj+=2;
	addButton(aboutButton, idj, title[count++], 0, 0,mScreenW, mScreenH/15, TEXTSIZE+20, RGB(0, 0, 0), "about2.jpg");
	idj+=2;
	addButton(aboutButton, ido, title[count++], mScreenW/40, mScreenH/100,mScreenW/15*2, mScreenH/15-mScreenH/100, TEXTSIZE+10, RGB(0, 0, 0), "about2.jpg");
	ido+=2;

}
void involve(double degrees,double px,double py,double x,double y)//旋转
{
	
}

void aboutCreate()	//在关于界面启动时调用
{
	Deart=NULL;
	struct DearT* p=Deart;
	aboutButton = NULL;
	aboutButton=initialButton();//初始化
/*	for(int x=0;x<10;x++)
	{
		p=(DearT*)malloc(sizeof(DearT));
		p->x=rand()%mScreenW/50*48+mScreenW/50;
		p->y=rand()%mScreenH+mScreenH/10;
		p->D=rand()%mScreenW/50+mScreenW/100;
		p->next=NULL;
		p=p->next;
	}*/
}
void aboutSizeChange(int w,int h,int oldw,int oldh,float density)//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
{
	mScreenW=w;
	mScreenH=h;
	mScreenD=density;
	clearButton(aboutButton);
	aboutpage(); //界面
}
void aboutLoopCall()	//每1毫秒就会调用这个方法1次
{
		   static long long proTime=0;
	//每200毫秒执行一次
	if(currentTimeMillis()-proTime>200)
	{
		proTime=currentTimeMillis();		
		postInvalidate();//通知系统重新绘图
	}
  
}
int aboutBackPressed()//返回键按下被调用
{
	aboutDestroy();
	funnumber=0;
	functiononCreate(funnumber);	//在界面启动时调用
	prefunnumber=funnumber;
}
void aboutDestroy()//界面销毁被调用
{
	clearButton(aboutButton);
	delteButton(&aboutButton);
}