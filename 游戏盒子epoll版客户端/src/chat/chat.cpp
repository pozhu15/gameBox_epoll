//
#include <fstream>
#include <iostream>
#include "../function.h"
#include "chat.h"
using namespace std;


void onClick(View view,void*data)
{
	chatBackPressed();
}
void sendonClick(View view,void*data)
{
	char*messbuf = TextView_getTextEx(message);//得到用户名编辑框内容
	if(strlen(messbuf)>0xA000)
	{
		showToastText("消息过长",0);
		return;
	}
	else if(strlen(messbuf)!=0)
		//  showToastText("消息不能为空",0);
	//  else
	{
		
		//记录消息到队
		epmessage recmess = fillmess(MESSAGE,client.getmymess().uname,2,messbuf);
		client.sendmess(recmess);
		//记录消息到队列
		UMessageLite recrovemess;
		strcpy(recrovemess.name,client.getmymess().uname);
		strcpy(recrovemess.Message,messbuf);
		UserMagList.push_front(&recrovemess);
		if(UserMagList.size()>200)//最大存储200条消息
			UserMagList.pop_back();
		
		//显示消息
		MessBoxs viewmess(client.getmymess().uname,messbuf,UMessDisplayQ.size()>0?(UMessDisplayQ.back()).GetId():0);
		UMessDisplayQ.push(viewmess);
		if(UMessDisplayQ.size()>50)//最大存储100条消息
		{
			UMessDisplayQ.front().cleanBoxs();
			UMessDisplayQ.pop();
		}
	}
	TextView_setTextEx(message,"");
}

void chatCreate()//界面启动时调用
{
	IdNumber = 500;
	//获取屏幕宽度
	mScreenW = getScreenWidth();
	//屏幕高度
	mScreenH = getScreenHeight();
   if(client.getmymess().status!=2) 
	client.sendmessage(CHANGESTATUS,client.getmymess().uname,2,(char*)"chamge");//改变状态
 
	
	mainRellayout = newLinearLayout();// 主页面线性布局
	topRellayout = newRelativeLayout();// 顶部状态栏布局
	mainmessRellayout = newScrollView();// 主消息显示框布局
	sendRellayout = newRelativeLayout();//发送消息布局
	menubarRellayout = newRelativeLayout();//菜单条布局
	backbutton = newButton();//返回按钮
	topmess = newTextView();//顶部信息条
	message = newEditText();//消息编辑框
	sendbutton = newButton();//发送按钮
	
	setContentView(mainRellayout);//设置主控件
	
	// View_setX(button2,220);//设置X位置
	//rectl.right = View_getWidth(Rellayout);
	// rectl.buttom = View_getHeight(Rellayout);
	//  TextView_setTextEx(button1,"button1");//顶部状态栏
	TextView_setTextEx(backbutton,"<");
	TextView_setTextSize(backbutton,15);//设置字体大小
	View_setBackgroundColor(backbutton,RGB(34,139,34));
	View_setOnClickListener(backbutton,onClick,NULL);
	TextView_setTextEx(sendbutton,"发送");
	TextView_setTextColor(sendbutton,RGB(0,19,255));
	View_setOnClickListener(sendbutton,sendonClick,NULL);
	View_setBackgroundColor(mainRellayout,RGB(155,15,25));
	View_setBackgroundColor(sendbutton,RGB(102,205,0));
	View_setBackgroundColor(topRellayout,RGB(15,155,25));
	View_setBackgroundColor(mainmessRellayout,RGB(124,205,125));
	View_setBackgroundColor(sendRellayout,RGB(162,205,90));
	View_setBackgroundColor(menubarRellayout,RGB(15,155,25));
	
	mainmessbitmap = decodeBitmapFromAssets("chatimg.jpg");
	//设置控件的背景图片
	View_setBackgroundImage(mainmessRellayout,mainmessbitmap);
	
	TextView_setTextEx(topmess,"在线人数");
	TextView_setTextSize(topmess,15);//设置字体大小
	
	//View_setBackgroundColor(Rellayout,RGB(255,255,255));
	// Bitmap backimg=createBitmap(200,300);
	//  backimg=decodeBitmapFromAssets("beijing.jpg");
	// View_setBackgroundImage(Rellayout,backimg);
	//  View_setX(editmessage,120);//设置编辑框X位置
	// View_setY(editmessage,815);//设置编辑框Y位置
	
	TextView_setTextEx(message,"");//设置编辑框内文字
	TextView_setTextSize(message,15);//设置编辑框字体大小
	//设置线性布局方向
	LinearLayout_setOrientation(mainRellayout,VERTICAL);
	
	mainparams = newRLayoutParams(FILL_PARENT,FILL_PARENT);//主页面布局参数
	topbarparams = newRLayoutParams(WRAP_CONTENT,WRAP_CONTENT);//顶部状态栏相对布局参数
	topmessparams = newRLayoutParams(WRAP_CONTENT,WRAP_CONTENT);//顶部消息相对布局参数
	backparams = newRLayoutParams(WRAP_CONTENT,WRAP_CONTENT);//返回按钮相对布局参数
	mainmessparams = newLayoutParams(WRAP_CONTENT,WRAP_CONTENT);//主消息布局参数
	sendbuttonparams = newRLayoutParams(WRAP_CONTENT,WRAP_CONTENT);//发送按钮相对布局参数
	messeditparams = newRLayoutParams(WRAP_CONTENT,WRAP_CONTENT);//message相对布局参数
	sendmessbarparams = newRLayoutParams(WRAP_CONTENT,WRAP_CONTENT);//底部发送状态栏相对布局参数
	menubarparams = newRLayoutParams(WRAP_CONTENT,WRAP_CONTENT);//底部菜单条相对布局参数
	
	LayoutParams_setWidth(topbarparams,FILL_PARENT);//设置布局参数宽
	LayoutParams_setHeight(topbarparams,mScreenH/15);
	LayoutParams_setWidth(topmessparams,mScreenW/3);//设置布局参数宽
	//  LayoutParams_setHeight(topmessparams,mScreenH/15);
	LayoutParams_setWidth(backparams,mScreenW/10);//设置布局参数宽
	LayoutParams_setHeight(backparams,mScreenH/15);
	LayoutParams_setWidth(mainmessparams,FILL_PARENT);//设置布局参数宽
	LayoutParams_setHeight(mainmessparams,mScreenH/5*4);
	LayoutParams_setWidth(sendbuttonparams,mScreenW/5);//设置布局参数宽
	LayoutParams_setHeight(sendbuttonparams,mScreenH/15);
	LayoutParams_setWidth(messeditparams,mScreenW/5*4);//设置布局参数宽
	LayoutParams_setHeight(messeditparams,mScreenH/15);
	LayoutParams_setWidth(sendmessbarparams,FILL_PARENT);//设置布局参数宽
	LayoutParams_setHeight(sendmessbarparams,mScreenH/15);
	LayoutParams_setWidth(menubarparams,FILL_PARENT);//设置布局参数宽
	LayoutParams_setHeight(menubarparams,mScreenH/15);
	
	RLayoutParams_addRule(topbarparams,RL_ALIGN_PARENT_TOP,RL_TRUE);//对齐父布局的上边
	RLayoutParams_addRule(topmessparams,RL_CENTER_IN_PARENT,RL_TRUE);//在父布局中居中
	RLayoutParams_addRule(backparams,RL_ALIGN_PARENT_LEFT|RL_CENTER_VERTICAL,RL_TRUE);//对齐父布局的左边
	RLayoutParams_addRule(sendbuttonparams,RL_ALIGN_PARENT_RIGHT,RL_TRUE);// 对齐父布局的右边
	RLayoutParams_addRule(messeditparams,RL_ALIGN_PARENT_LEFT,RL_TRUE);//对齐父布局的左边
	RLayoutParams_addRule(sendmessbarparams,RL_ALIGN_TOP,RL_TRUE);//对齐谁的上边
	RLayoutParams_addRule(menubarparams,RL_ALIGN_PARENT_BOTTOM,RL_TRUE);//对齐父布局的下边
	
	View_setLayoutParams(mainRellayout,mainparams);//设置布局参数
	View_setLayoutParams(topRellayout,topbarparams);//设置布局参数
	View_setLayoutParams(mainmessRellayout,mainmessparams);//设置布局参数
	View_setLayoutParams(sendRellayout,sendmessbarparams);//设置布局参数
	View_setLayoutParams(menubarRellayout,menubarparams);//设置布局参数
	View_setLayoutParams(backbutton,backparams);//设置布局参数
	View_setLayoutParams(topmess,topmessparams);//设置布局参数
	View_setLayoutParams(message,messeditparams);//设置布局参数
	View_setLayoutParams(sendbutton,sendbuttonparams);//设置布局参数
	
	ViewGroup_addView(mainRellayout,topRellayout);//添加View
	ViewGroup_addView(mainRellayout,mainmessRellayout);
	// ViewGroup_addView(mainmessRellayout,scrolltest);//添加View
	
	//消息气泡
	MessBLayout = newRelativeLayout();
	MessBtParam = newRLayoutParams(WRAP_CONTENT,WRAP_CONTENT);//相对布局参数
	View_setLayoutParams(MessBLayout,MessBtParam);//设置布局参数
	ViewGroup_addView(mainmessRellayout,MessBLayout);//添加View
	
	ViewGroup_addView(mainRellayout,sendRellayout);
	ViewGroup_addView(mainRellayout,menubarRellayout);
	ViewGroup_addView(topRellayout,backbutton);//添加View
	ViewGroup_addView(topRellayout,topmess);
	ViewGroup_addView(sendRellayout,message);
	ViewGroup_addView(sendRellayout,sendbutton);
	 DealMessThreadProc();//显示消息
	
	chatbutton1 = newButton();//按钮
	chatbutton2 = newButton();//按钮
	chatbutton3 = newButton();//按钮
	chatbutton4 = newButton();//按钮
	TextView_setTextEx(chatbutton1,"*");
	TextView_setTextSize(chatbutton1,15);//设置字体大小
	TextView_setTextEx(chatbutton2,"*");
	TextView_setTextSize(chatbutton2,15);//设置字体大小
	TextView_setTextEx(chatbutton3,"*");
	TextView_setTextSize(chatbutton3,15);//设置字体大小
	TextView_setTextEx(chatbutton4,"*");
	TextView_setTextSize(chatbutton4,15);//设置字体大小
	
	
	/*    buttonparams1;
	 buttonparams2;
	 buttonparams3;
	 buttonparams4;*/
	
	postInvalidate();
}

void chatTouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[])//触屏事件
{
	
}
void chatDraw(int left,int top,int right,int bottom,Canvas canvas)//画图		
{
	/*   Canvas_drawColor(canvas,RGB(255,255,255));
	Paint paint = newPaint();//创建画笔
	Paint_setColor(paint,RGB(0,0,0));//设置画笔颜色
	Canvas_drawPoint(canvas,10,10,paint);//画点
	Canvas_drawLine(canvas,0,100,100,100,paint);//画线
	Canvas_drawRect(canvas,0,200,200,400,paint);//画矩形
	Paint_setStyle(paint,STYLE_STROKE);//设置画笔为中空样式
	Canvas_drawRoundRect(canvas,0,600,200,800,60,60,paint);//画圆角矩形
	Canvas_drawCircle(canvas,400,200,100,paint);//画圆
	Paint_setTextSize(paint,32);//设置文字大小
	Paint_setTypeface(paint,TYPEFACE_MONOSPACE);//设置为等宽字体
	Canvas_drawText(canvas,"Hello world!",500,500,paint);//绘制文本
	deletePaint(paint);//释放画笔
	*/
}
void chatLoopCall()//每1毫秒就会调用这个方法1次
{
	while(client.getrecemesslistsize())//接受消息队列不为空则输出消息
	{
		epmessage messg = client.recemess();//取出接收队列中的消息，前提是先验证队列不为空
			//显示消息
			MessBoxs viewmess(messg.uname,messg.mess,UMessDisplayQ.size()>0?(UMessDisplayQ.back()).GetId():0);
			UMessDisplayQ.push(viewmess);
			if(UMessDisplayQ.size()>100)//最大存储100条消息
			{
				UMessDisplayQ.front().cleanBoxs();
				UMessDisplayQ.pop();
			}
	}
	static long long prochatTime = 0;
	//每200毫秒执行一次
	if(currentTimeMillis()-prochatTime>500)
	{
		prochatTime = currentTimeMillis();
		char onlinenum[15];
		sprintf(onlinenum,"在线人数 %d人",client.getmymess().Clientnum);
		TextView_setTextEx(topmess,onlinenum);
		postInvalidate();//通知系统重新绘图
	}
}
void chatSizeChange(int w,int h,int oldw,int oldh,float density)//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
{
	
}
int chatBackPressed()//返回键按下被调用
{
	chatDestroy();
	funnumber = 0;
	functiononCreate(funnumber);//在界面启动时调用	
	prefunnumber = funnumber;
	postInvalidate();//通知系统重新绘图
}
void chatDestroy()//界面销毁被调用
{
	ViewGroup_removeAllViews(mainRellayout);//移除全部子视图
	// deleteRelativeLayout(mainRellayout);//删除布局
	ViewGroup_removeAllViews(topRellayout);//移除全部子视图
	deleteRelativeLayout(topRellayout);//删除布局
	ViewGroup_removeAllViews(sendRellayout);//移除全部子视图
	deleteRelativeLayout(sendRellayout);//删除布局
	ViewGroup_removeAllViews(mainmessRellayout);//移除全部子视图
	deleteScrollView(mainmessRellayout);//删除布局
	ViewGroup_removeAllViews(menubarRellayout);//移除全部子视图
	deleteRelativeLayout(menubarRellayout);//删除布局
	View_setVisibility(mainRellayout,GONE);
	
	//滚动布局
	ViewGroup_removeAllViews(MessBLayout);//移除全部子
	//  deleteRelativeLayout(MessBLayout);//删除布局
	View_setVisibility(MessBLayout,GONE);
	
	//消息气泡
	ViewGroup_removeAllViews(MessBLayout);//移除全部子
	//  deleteRelativeLayout(MessBLayout);//删除布局
	View_setVisibility(MessBLayout,GONE);
	deleteRLayoutParams(MessBtParam);
	
	deleteRLayoutParams(mainparams);
	deleteRLayoutParams(topbarparams);
	deleteRLayoutParams(topmessparams);
	deleteRLayoutParams(backparams);
	deleteRLayoutParams(mainmessparams);
	deleteRLayoutParams(sendbuttonparams);
	deleteRLayoutParams(messeditparams);
	deleteRLayoutParams(sendmessbarparams);
	deleteRLayoutParams(menubarparams);
	
	
	deleteTextView(topmess);
	deleteEditText(message);
	deleteButton(backbutton);
	deleteButton(sendbutton);
	
	
	deleteBitmap(mainmessbitmap);
	//  deleteBitmap(mainmessbitmap);
	
	while(UMessDisplayQ.size())//清空队列
	{
		UMessDisplayQ.front().cleanBoxs();
		UMessDisplayQ.pop();
		
	}
}
