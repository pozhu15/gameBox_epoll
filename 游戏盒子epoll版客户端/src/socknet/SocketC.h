#ifndef _SOCKETC_H
#define _SOCKETC_H
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include<string.h>
#include<conio.h>
#include<pthread.h>



#define Msgle 1024//最大消息长度
#define MessLong 200	//消息队列最大长度
#define Mlong sizeof(struct message)//传送得消息总大小
#define messL sizeof(struct messagelist)	//消息队列结构体长度


struct sockaddr_in clientAddr;

typedef struct message{
char from[20]; //发送者个人ID
char to[20]; //接收者ID
char name[30];//昵称
char Time[20];
int num;      //执行码
int concrol[10];//命令
int length;    //发送的消息主体的长度
char mess[Msgle];//消息和代码部分
} Item;
/*
typedef struct message{
char from[20]; //发送者个人ID
char to[20]; //接收者ID
char name[30];//昵称
char Time[20];
int num;      //执行码
int concrol[10];//命令
int length;    //发送的消息主体的长度
char mess[Msgle];//消息和代码部分
} Item;
*/
struct messagelist{		//消息队列
	int isr;
	struct message Msg;		//message消息体
	struct messagelist* next;
};
typedef struct messagelist* PNode;//消息队列
struct  Queue		//队列
{
	struct messagelist* front;  //
	struct messagelist* rear;  //
	int size;				//消息队列长度
};

Item fillmess(char* a);//填充消息块
void socketinit(int* CSocket);//初始化套接字
int connectS(int* socketfd);//连接服务器
void sendS(int* socketfd,Item Message);//发送数据
Item  receiveS(int* socketfd);//接受数据
void closesocket(int* socka);//关闭套接字

//队列
/*
Queue *InitQueue();					//构造一个空队列
void DestroyQueue(Queue *pqueue);	//销毁一个队列
void ClearQueue(Queue *pqueue);	//清空一个队列
int IsEmpty(Queue *pqueue);		//判断队列是否为空
int GetSize(Queue *pqueue);		//返回队列大小
PNode GetFront(Queue *pqueue, Item *pitem);	//返回队头元素
PNode GetRear(Queue *pqueue, Item *pitem);	//返回队尾元素
PNode EnQueue(Queue *pqueue, Item item);	//将新元素入队
PNode DeQueue(Queue *pqueue, Item *pitem);	//队头元素出队
PNode IsOverLimited(Queue *pqueue);	//判断是否超过消息队列最大长度，超出则删除前面已读元素
*/


#endif
