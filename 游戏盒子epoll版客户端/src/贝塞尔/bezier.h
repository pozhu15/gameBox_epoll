#ifndef BEZIER_H
#define BEZIER_H
#define BEZIERJ 5 //曲线阶数
//extern "C"
//#define LOG_TAG "main"

typedef struct Point{
	//定义点
	double x;
	double y;
	struct Point*next;
}point;

typedef struct Bezier{
	//定义链表
	point*phead;
	point*pend;
	int count;
}bezierP;

void initbez(bezierP*bp);//初始化链表

extern bezierP bepo;//所有点
extern bezierP bebuf;//中介点
extern int SCRW ;
extern int SCRH ;
	
void bezierCreate();	//界面启动时调用
void bezierTouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[]);//触屏事件
void bezierDraw(int left,int top,int right,int bottom,Canvas canvas);		//画图		
void bezierLoopCall();	//每1毫秒就会调用这个方法1次
void bezierSizeChange(int w,int h,int oldw,int oldh,float density);//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
int  bezierBackPressed();//返回键按下被调用
void bezierDestroy();//界面销毁被调用


point*getp(bezierP*pot,int num);//获得点
void changp(bezierP*pot,int num,double x,double y);//修改点
void addp(bezierP*po,double x,double y);//末尾添加点
void delendp(bezierP*po);//删除末尾点
void clearbezier(bezierP*po);//清空链表

//在起点为(x1,y1)，终点为(x2,y2)的线段上移动长度为long,得到点(x,y),和所占比例k
void stepgetpbezier(double x1,double y1,double x2,double y2,double lon,double*x,double*y,double*k);
//在起点为(x1,y1)，终点为(x2,y2)的线段上所占比例为k,得到点(x,y)
void kgetp(double x1,double y1,double x2,double y2,double k,double*x,double*y);

void findp(bezierP*bp,double lon,double*x,double*y);//在bp点集中找出第一段移动距离为lon的贝塞尔点(x,y)
int findjinpbezier(bezierP*bp,float x,float y);//找到与点(x,y)最近的点节点




#endif