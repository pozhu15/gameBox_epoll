//
#include "../function.h"


RelativeLayout testtempRellayout;
TextView testtemptextview;
RLayoutParams testtempparams;
Bitmap testmessbitmap;

void Create()//界面启动时调用
{
	testtempRellayout = newRelativeLayout();// 主页面布局
	setContentView(testtempRellayout);
	
	testtemptextview = newTextView();//信息条
	testmessbitmap = decodeBitmapFromAssets("backimg3.jpg");
	//设置控件的背景图片
	View_setBackgroundImage(testtempRellayout,testmessbitmap);
	//相对布局参数
	testtempparams = newRLayoutParams(WRAP_CONTENT,WRAP_CONTENT);
	//在父视图中居中
	RLayoutParams_addRule(testtempparams,RL_CENTER_IN_PARENT,RL_TRUE);
	View_setLayoutParams(testtemptextview,testtempparams);
	ViewGroup_addView(testtempRellayout,testtemptextview);
	
}
void TouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[])//触屏事件
{
	
}
void Draw(int left,int top,int right,int bottom,Canvas canvas)//画图		
{
	
}
void LoopCall()//每1毫秒就会调用这个方法1次
{
	static long long proTime = 0;
	static int i = 0;
	static int fortsize = 20;
	//每100毫秒执行一次
	if(currentTimeMillis()-proTime>1)
	{
		proTime = currentTimeMillis();
		char*testtext[] = {
			"你","瞅","啥",
			"？","啥","都","没","有",
			"！"
		}; 
		
		TextView_setTextEx(testtemptextview,testtext[i]);
		TextView_setTextSize(testtemptextview,fortsize);//设置字体大小
		postInvalidate();//通知系统重新绘图
		fortsize+=8;
		if(fortsize>150)
		{
			fortsize = 20;
			i++;
			if(i>8)
				i = 0;
		}
	}
	if( funnumber==0)
	fortsize=20;
}
void SizeChange(int w,int h,int oldw,int oldh,float density)//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
{
	
}
int BackPressed()//返回键按下被调用
{
	Destroy();
	funnumber = 0;
	functiononCreate(funnumber);//在界面启动时调用	
	prefunnumber = funnumber;
	postInvalidate();//通知系统重新绘图
	return 0;
}
void Destroy()//界面销毁被调用
{
	ViewGroup_removeAllViews(testtempRellayout);
	deleteRLayoutParams(testtempparams);
	deleteTextView(testtemptextview);
	View_setVisibility(testtempRellayout,GONE);
	deleteBitmap(testmessbitmap);
}