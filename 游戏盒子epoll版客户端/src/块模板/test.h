#ifndef TEST_H
#define TEST_H


 extern  RelativeLayout testtempRellayout ;// 主页面布局
  extern TextView testtemptextview;//信息条
  extern RLayoutParams testtempparams;//布局参数
 extern Bitmap testmessbitmap;
	
void Create();	//界面启动时调用
void TouchEvent(int action,float x,float y,int index,int count,float pointersX[],float pointersY[],int pointersId[]);//触屏事件
void Draw(int left,int top,int right,int bottom,Canvas canvas);		//画图		
void LoopCall();	//每1毫秒就会调用这个方法1次
void SizeChange(int w,int h,int oldw,int oldh,float density);//屏幕大小发生变化时被调用 ，程序在启动后会调用一次
int  BackPressed();//返回键按下被调用
void Destroy();//界面销毁被调用

#endif