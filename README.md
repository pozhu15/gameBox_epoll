# gameBox_epoll

#### 介绍
c++游戏盒子基于epoll实现高并发，功能有聊天室，ai五子棋，网与星特效，贪吃蛇等游戏

#### 软件架构
服务器基于linux，客户端图形库基于手机c++编程器快写代码，通过epoll，线程，管道pipe通信，多消息队列，实现高并发聊天室


#### 安装教程
前提需要c++环境

1.  服务器编译，运行
    cd  进入服务器源码目录

    g++ Server.cpp ServerMain.cpp -o sc -lpthread

   运行
    ./sc

3.  客户端在手机IDE快写代码打包运行

#### 使用说明
需要修改客户端源代码中的服务器IP地址，再运行客户端
先启动服务器，然后打开客户端


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
