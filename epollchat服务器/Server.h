#ifndef CHATROOM_SERVER_H
#define CHATROOM_SERVER_H

#include <string>

#include "Common.h"

using namespace std;

typedef struct usermess	//服务器中存储的用户信息
{
	int socket;		//套接字
	char uname[20];	//用户
	char passw[20];	//密码
	int status;	//用户状态，0连接但未登录，1连接且仅在线状态，2聊天室...
	int group; 		//用户组，0为未活动
}userms;

epmessage fillmess(int style,char* name,int sta,char* mess);	//填充消息
void outpmess(epmessage messb);	//输出消息内容

// 服务端类，用来处理客户端请求
class Server {

public:
    // 无参数构造函数
    Server();

    // 初始化服务器端设置
    void Init();

    // 关闭服务
    void Close();

    // 启动服务端
    void Start();
	
	bool sockdeluser(int soc, char * name);	//通过socket删除用户
	userms sockfinduser(int soc);	//通过socket找出用户
	bool isuserbecode(int soc, char * name);	//查找socket，名称为name的用户是否存在,存在返回1
	
	int setgroup(int status1);	//通过状态得到可以设置的组号
	bool sendmessage(int sock,int style,char* name,int sta,char* mess);	//发送消息,成功返回true

private:
    // 广播消息给所有客户端
    int SendBroadcastMessage(int clientfd);

    // 服务器端serverAddr信息
    struct sockaddr_in serverAddr;
    
    //创建监听的socket
    int listener;

    // epoll_create创建后的返回值
    int epfd;
    
    list<userms> clients_list;  // 客户端列表
	queue<epmessage> epmessage_list; //消息队列
};



#endif //CHATROOM_SERVER_H
