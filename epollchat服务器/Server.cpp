#include "Server.h"

// 服务端类成员函数

epmessage fillmess(int style,char* name,int sta,char* mess)	//填充消息
{
	epmessage messbuf;
	bzero(&messbuf, sizeof(epmessage));
	messbuf.emstyle=style;
	strcpy(messbuf.uname,name);
	messbuf.status=sta;
	memcpy(messbuf.mess,mess,strlen(mess));
	return messbuf;
}
void outpmess(epmessage messb)	//输出消息内容
{
	cout<<"##################### message#############################"<<endl;
	cout<<"message stype:"<<messb.emstyle<<endl; //消息类型
	cout<<"username:"<<messb.uname<<endl; 
	cout<<"status:"<<messb.status<<endl; 
	cout<<"message body:"<<messb.mess<<endl; 
}

// 服务端类构造函数
Server::Server(){
    
    // 初始化服务器地址和端口
    serverAddr.sin_family = PF_INET;
    serverAddr.sin_port = htons(SERVER_PORT);
    serverAddr.sin_addr.s_addr = inet_addr(SERVER_IP);

    // 初始化socket
    listener = 0;
    
    // epool fd
    epfd = 0;
}

// 初始化服务端并启动监听
void Server::Init() {
    cout << "Init Server..." << endl;
    
     //创建监听socket
    listener = socket(PF_INET, SOCK_STREAM, 0);
    if(listener < 0) { perror("listener"); exit(-1);}
    
    //绑定地址
    if( bind(listener, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0) {
        perror("bind error");
        exit(-1);
    }

    //监听
    int ret = listen(listener, 5);
    if(ret < 0) {
        perror("listen error"); 
        exit(-1);
    }

    cout << "Start to listen: " << SERVER_IP << endl;

    //在内核中创建事件表
	/*
	epoll_create(int size);
	创建一个epoll的句柄，size用来告诉内核这个监听的数目最大值。
	当创建好epoll句柄后，它就是会占用一个fd值，所以在使用完epoll后，
	必须调用close()关闭，否则可能导致fd被耗尽。
	*/
    epfd = epoll_create(EPOLL_SIZE);
    
    if(epfd < 0) {
        perror("epfd error");
        exit(-1);
    }

    //往事件表里添加监听事件
    addfd(epfd, listener, true);

}

// 关闭服务，清理并关闭文件描述符
void Server::Close() {

    //关闭socket
    close(listener);
    
    //关闭epoll监听
    close(epfd);
}

// 发送广播消息给所有客户端
int Server::SendBroadcastMessage(int clientfd)
{
    // buf[BUF_SIZE] 接收新消息
    // message[BUF_SIZE] 保存格式化的消息
    char buf[sizeof(epmessage)];
	epmessage message;	
    bzero(buf, sizeof(epmessage));
    bzero(&message,  sizeof(epmessage));

    // 接收新消息
    //cout << "read from client(clientID = " << clientfd << ")" << endl;
    int len = recv(clientfd, buf,  sizeof(epmessage), 0);

    // 如果客户端关闭了连接
    if(len == 0)
    {
        close(clientfd);
        
        // 在客户端列表中删除该客户端
        //clients_list.remove(sockfinduser(clientfd));
        if(sockdeluser(clientfd,sockfinduser(clientfd).uname))
		{
			 cout << "*******************client login out***********************"<<endl;
			cout <<"\tclientfd="<< clientfd <<"\n\tname="<<sockfinduser(clientfd).uname<< "\n " 
				 << "Number of people who online:"<< clients_list.size()<< endl;
		}
		else
		{
			cout << "*******************delete user client failed***********************"<<endl;
			cout<<sockfinduser(clientfd).uname<<endl;
		}
    }
    // 发送广播消息给所有客户端
    else 
    {
        // 判断是否聊天室还有其他客户端
       // if(clients_list.size() == 1) { 
            // 发送提示消息
        //    send(clientfd, CAUTION, strlen(CAUTION), 0);
        //    return len;
        ///}
        // 格式化发送的消息内容
        //sprintf(message, SERVER_MESSAGE, clientfd, buf);
		memcpy(&message,buf,sizeof(epmessage));

       
		if(message.emstyle>=1000&&message.emstyle<1500&&strlen(message.uname)>0)//过滤类型不对的消息
		{
			cout<<"user *********************:"<<endl;
					cout<<"sockfinduser socket:"<<sockfinduser(clientfd).socket<<endl;
					cout<<"sockfinduser uname:"<<sockfinduser(clientfd).uname<<endl;
					cout<<"sockfinduser passw:"<<sockfinduser(clientfd).passw<<endl;
					cout<<"sockfinduser status:"<<sockfinduser(clientfd).status<<endl;
					cout<<"sockfinduser group:"<<sockfinduser(clientfd).group<<endl;
			outpmess(message);
			
			if(message.emstyle==LOGOUT)	//判断消息是否为退出消息
			{
				list<userms>::iterator it;
				for(it = clients_list.begin();it != clients_list.end();++it)
				{
					 sockdeluser(clientfd,message.uname);
					 close(clientfd);
					//客户端人数变化，通知人数
					if(!sendmessage(it->socket,CLIENTCHANTESAYNUM,message.uname,clients_list.size(),(char*)"login out"))	//发送消息
					{
						perror("send error");
					}		
				}
			}
			if(message.emstyle==LOGIN)	//登陆消息
			{
				list<userms>::iterator it;
				for(it = clients_list.begin(); it != clients_list.end(); ++it) 
				{
					if(it->socket == clientfd)
					 {
						if(1)	//账号密码正确
						{
							strcpy(it->uname,message.uname);
							strcpy(it->passw,message.mess);
							it->status=1;
							it->group=0;
							 
							cout << "*******************new user succeed***********************"<<endl;
							cout << "\tclientfd = " << clientfd << "\n\tname = " << message.uname << endl;
							cout << "\tNumber of people who online:" << clients_list.size() << endl;
							
							// 服务端发送登陆成功信息  	
							char messbuf[BUF_SIZE];
							bzero(messbuf, BUF_SIZE);
							sprintf(messbuf, SERVER_WELCOME, clients_list.size());
							if(!sendmessage(clientfd,LOGINSUCCEED,(char*)"system",1,messbuf))	//发送消息
							{
								perror("send error");
							}
							  	//客户端人数变化，通知人数
							list<userms>::iterator it1;
							for(it1 = clients_list.begin(); it1 != clients_list.end(); ++it1) 
							{
								if(!sendmessage(it1->socket,CLIENTCHANTESAYNUM,message.uname,clients_list.size(),(char*)"system"))	//发送消息
								{
									perror("send error");
								}
							}
							break;
						}
						else //登陆验证不成功
						{
							char messbuf[BUF_SIZE];
							bzero(messbuf, BUF_SIZE);
							sprintf(messbuf, "login error");
							if(!sendmessage(clientfd,LOGINERROR,(char*)"system",0,messbuf))	//发送消息
							{
								perror("send error");
							}
							cout << "*******************login failed***********************"<<endl;
							cout<< "\t, clientfd = "
								 << clientfd
								 <<"  login falied!"<< endl;
							break;
						}
					 }
				}
			}
			if(message.emstyle==CHANGESTATUS)	//判断消息是否为改变状态消息
			{
				list<userms>::iterator it;
				for(it = clients_list.begin(); it != clients_list.end(); ++it) 
				{
					 if(it->socket == clientfd&&strcmp(it->uname,message.uname)==0)
					 {
						 it->status=message.status;
						 it->group=setgroup(message.status);
						 // 服务端发送改变状态成功信息  	
						char messbuf[BUF_SIZE];
						bzero(messbuf, BUF_SIZE);
						sprintf(messbuf, SERVER_WELCOME, clientfd);
						if(!sendmessage(clientfd,CHANGESTATUSSUCCEED,(char*)"system",message.status,messbuf))	//发送消息
						{
							perror("send error");
						}
						break;
					 }
				}
			}
			/*if(message.emstyle==MESSAGE&&message.status <1)//如果存在状态为0的异常消息
			{
				if(!isuserbecode(clientfd,message.uname))	//如果没有注册则先注册
				{
					userms userbuf;
					bzero(&userbuf, sizeof(userms));
					userbuf.socket=clientfd;
					strcpy(userbuf.uname,message.uname);
					strcpy(userbuf.passw,message.uname);
					userbuf.status=message.status;	//连接，未登录状态
					userbuf.group=setgroup(message.status);	//未活动
					// 服务端用list保存用户连接
					clients_list.push_back(userbuf);
				}
				cout<<"状态为0的异常消息"<<endl;
				return len;
			}
			if(message.emstyle==MESSAGE&&message.status ==1)//如果存在状态为1的异常消息
			{
				cout<<"状态为1的异常消息"<<endl;
				return len;
			}*/
			 // 遍历客户端列表依次发送消息，需要判断不要给来源客户端发,且发送给同一用户组
			 if(message.emstyle==MESSAGE)
			{
				int issendsucceed=0;	//是否发送成功
				int islo=0;	//是否登陆
				list<userms>::iterator it;
				for(it = clients_list.begin(); it != clients_list.end(); ++it) 
				{
					if(it->group>0&&it->status>1)	//发送消息给其他用户
					{
						cout<<"user ************ massage:"<<endl;
						cout<<"socket:"<<it->socket<<endl;
						cout<<"uname:"<<it->uname<<endl;
						cout<<"passw:"<<it->passw<<endl;
						cout<<"status:"<<it->status<<endl;
						cout<<"group:"<<it->group<<endl;
						if((it->socket != clientfd||(it->socket == clientfd&&strcmp(it->uname,message.uname)!=0))&&it->status==message.status&&it->group==sockfinduser(clientfd).group)
						{
							if(!sendmessage(it->socket,message.emstyle,message.uname,message.status,message.mess))	//发送消息
							{
								perror("send error");
							}
							
						}
						if(it->socket == clientfd)
						{
							issendsucceed++;
							if(strcmp(it->uname,message.uname)==0)
							{
								islo++;
							}
						}
					}
					else
					{
						;//cout<<"状态为"<<的异常消息"<<endl;
					}
				}
				if(issendsucceed==0)	//没有成功登陆或已掉线的消息
				{
					char messbuf[BUF_SIZE];
					bzero(messbuf, BUF_SIZE);
					sprintf(messbuf, "you havenot logined in!");
					if(!sendmessage(clientfd,SERVERHAVENOTYOU,(char*)"system",0,messbuf))	//发送消息
					{
						perror("send error");
					}
				}
				else if(issendsucceed>1)	//重复登陆
				{
					if(islo==1)
					for(it = clients_list.begin();it != clients_list.end();++it)
					{
						if(it->socket == clientfd&&strcmp(it->uname,message.uname)!=0)	//如果用户名不相等删除用户
						{
							//sockdeluser(it->socket,it->uname);
						}
					}
				}
			}
		}
    }
    return len;
}
bool Server::sockdeluser(int soc,char * name)//通过socket套接字删除用户
{
	list<userms>::iterator it;
	for(it = clients_list.begin(); it != clients_list.end(); ++it) {
	   if(it->socket==soc&&strcmp(it->uname,name)==0)
	   {
		clients_list.erase(it);
        return true;
	   }
	}
    return false;
}
userms Server::sockfinduser(int soc)	//通过socket找出用户
{
	userms ubuf;
	list<userms>::iterator it;
	for(it = clients_list.begin(); it != clients_list.end(); ++it) 
	{
	   if(it->socket==soc)
	   {
			ubuf.socket=it->socket;
			stpcpy(ubuf.uname,it->uname);
			stpcpy(ubuf.passw,it->passw);
			ubuf.status=it->status;
			ubuf.group=it->group;
			return ubuf;
	   }
	}
	return ubuf;
}
bool Server::isuserbecode(int soc, char * name)	//查找socket，名称为name的用户是否存在,存在返回1
{
	list<userms>::iterator it;
	for(it = clients_list.begin(); it != clients_list.end(); ++it) 
	{
	   if(it->socket==soc&&strcmp(it->uname,name)==0)
	   {
			return true;
	   }
	}
	return false;
}

bool Server::sendmessage(int sock,int style,char* name,int sta,char* mess)	//发送消息,成功返回true
{
	char buf1[sizeof(epmessage)];
	bzero(buf1, sizeof(epmessage));
	epmessage mege=fillmess(style,name,sta,mess);	
	memcpy(buf1,&mege,sizeof(epmessage));
	if( send(sock, buf1, sizeof(epmessage), 0) < 0 ) 
	{
		return false;
	}
	return true;
}

int Server::setgroup(int status1)	//通过状态得到可以设置的组号
{	
	list<userms>::iterator it;
	for(int grou=1;grou<1000;grou++)	//从第一组开始遍历,遍历100组
	{
		int num=0;
		for(it = clients_list.begin(); it != clients_list.end(); ++it) 
		{
		   if(it->status==status1)
		   {
				if(it->group==grou)
					num++;
		   }
		}
		if(num==0)
			return num+1;
		switch(status1)
		{
			case 2:		//聊天室
				if(num<1000)	//1000人一个群组
					return grou;
				break;
			case 3:		//五子棋
				if(num<2)		//2人一组
					return grou;
				break;
		}
	}
}
// 启动服务端
void Server::Start() {

    // epoll 事件队列
    static struct epoll_event events[EPOLL_SIZE]; 

    // 初始化服务端
    Init();

    //主循环
    while(1)
    {
        //epoll_events_count表示就绪事件的数目
		/*
		int epoll_wait(int epfd, struct epoll_event *events, int maxevents, int timeout);
		等待事件的产生，返回需要处理的事件的数量，并将需处理事件的套接字集合于参数events内，
		可以遍历events来处理事件。
		参数epfd为epoll句柄
		events为事件集合
		参数timeout是超时时间（毫秒，0会立即返回，-1是永久阻塞）。该函数返回需要处理的事件数目，
		如返回0表示已超时
		*/
        int epoll_events_count = epoll_wait(epfd, events, EPOLL_SIZE, -1);
        if(epoll_events_count < 0) {
            perror("epoll failure");
            //break;
        }

        //cout << "epoll_events_count =\n" << epoll_events_count << endl;

        //处理这epoll_events_count个就绪事件
        for(int i = 0; i < epoll_events_count; ++i)
        {
            int sockfd = events[i].data.fd;
            //新用户连接
            if(sockfd == listener)
            {
                struct sockaddr_in client_address;
                socklen_t client_addrLength = sizeof(struct sockaddr_in);
                int clientfd = accept( listener, ( struct sockaddr* )&client_address, &client_addrLength );

                cout << "*******************new user logined***********************"<<endl;
                cout<< "IP:"<< inet_ntoa(client_address.sin_addr) << "\n"
                     << "PORT:"<< ntohs(client_address.sin_port) << "\n"
                     << "socket:"<< clientfd<< "\n";
				
				userms userbuf;
				bzero(&userbuf, sizeof(userms));
				userbuf.socket=clientfd;
				strcpy(userbuf.uname,(char*)"none");
				strcpy(userbuf.passw,(char*)"none");
				userbuf.status=0;	//连接，未登录状态
				userbuf.group=0;	//未活动
				addfd(epfd, clientfd, true);

				// 服务端用list保存用户连接
				clients_list.push_back(userbuf);
				cout << "Add succeed and Number of people is:" << clients_list.size() << endl;
				cout << "**********************************************************"<<endl;

				// 服务端发送欢迎信息  			
				char messbuf[BUF_SIZE];
				bzero(messbuf, BUF_SIZE);
				sprintf(messbuf, "Welcome little brother or sister!");
				epmessage sendmess=fillmess(CONNECTSUCCEED,(char*)"system",0,messbuf);	//填充消息,0是未登录状态
				int ret = send(clientfd, &sendmess, sizeof(epmessage), 0);
				if(ret < 0) {
					perror("send error");
					continue;
				}

            }
            //处理用户发来的消息，并广播，使其他用户收到信息
            else 
			{   
                int ret = SendBroadcastMessage(sockfd);
                if(ret < 0) 
				{
                    perror("len error");
                    //Close();
                   // exit(-1);
                }
            }
        }
    }

    // 关闭服务
    Close();
}
